# Minion

:warning: Work in progress... use with caution!

Minion is a tool to build analysis pipelines using your favorite command-line
tools and *minion rules*. Minion is inspired by Makefile and Shell scripts.
It provides way to easily write a set of rules to perform processing for one or several
files.

Contrast to many other frameworks, there is really no API to comply with.
Minion is all about writing short shell script -like rules and making them work together.

Minion may come in future with some built-in rules, but you are encouraged to use Minion
for your automation needs by writing your own rules.

## Installing

Minion is currently only available through *gitlab*, install it like this:

    % pip3 install git+https://gitlab.com/cincan/minion

Use virtual environments etc. to your liking, Minion is just a regular Python application.

Documentation examples use heavily the `cincan` command-line tool
(available in PyPI, see https://gitlab.com/cincan/cincan-command) for invoking dockerized analysis tools.
Some examples also use `linemux` command-line tool, which is automatically installed with Minion.
However, __Minion can work with any command-line tools__.

### Hello world

There is minimal built-in rule set `hello.rules` to check the Minion. Invoke it like this:

    $ minion -cr hello.rules use
    08:51:07 [/] hello
    Hello, world!

## Minion rules

Minion works through **rules** collected into **rule files** with suffix `.rules`.
The default minion rule file is `minion.rules` in current directory.

Below we create a simple rule file, but you can use any of the built-in
rule files with `--rules` (or `-r`) option.

## Minimal rule file

A minimal Minion rule file could look like this
(the command `cat -n file` prints the file with line numbers):

    rule IN => print
         cat -n $IN

Create it as file`minion.rules` for examples below to work.

## Minion use -command

You process a file, using the above rules, like this:

    % minion use input.txt
    18:14:24 [x] input.txt.d/print
    18:14:24 cat -n input.txt > input.txt.d/print
    input.txt.d/print

Where `input.txt` is the file to process (in this case print with line numbers).
Without any given files, an error message is given, if the rule file requires input.
The last line gives the result output file.

As default, the use command prints the output files after a log of the commands.
Minion may print a lot of log, so it may be hard to know which is
the output file or files. The command-line option `-q` makes Minion quiet and
only to print the output file(s), or errors if things go wrong.

    % minion -q use
    input.txt.d/print

You can use command-line option `-c` to get the content of the output files instead of the
file names.
(make sure the files are printable).

    % minion -c use

Alternatively you can use option `--live-log` to see updated overview of the
rule evaluation. The normal log gets saved into file `.minion/log`.

    % minion --live-log use

## Built-in rules

If you do not provide any other rule file (with `--rules` or `-r`)
then the default rule file is used.
If the file does not exists, you get the list of built-in rule files
(if you have `minion.rules` in current directory, it is used and you do not get
list of built-in rules):

    % minion use
    unpack.rules
    samples/volatility.rules
    samples/scrape-website.rules
    samples/pdf-pipeline.rules

Currently there are the following built-in rule files:

| Rule file                      | Description             |
|--------------------------------|-------------------------|
| unpack.rules                   | Unpack some common archive formats |
| samples/volatility.rules       | Samples to use 'volatility' command |
| samples/scrape-website.rules   | Scrape website image by URL |
| samples/pdf-pipeline.rules     | Process PDF files |

As said, you can specify the rules file with `--rules` (or `-r`).
The rule files are searched from the current directory and from the Minion built-in rules.
For example, the following uses the rule set 'samples/pdf-pipeline.rules'
from built-in rules to process the file 'myfile.pdf'

    % minion -r samples/pdf-pipeline.rules use myfile.pdf
    ...
    summary.json

The analysis result is in 'summary.json', if all goes well.

## Cookbook

A Minion [rule cookbook](docs/Cookbook.md) is a collection of "recipes" - Minion rule
file snippets and their documentation.
First recipies are very simple, but there are also come more complex ones.
The cookbook might be a good place to get started with writing your own rules.

## Rule export

You can [export rule sets](docs/Export.md) e.g. into UML activity diagrams.

## Rule troubleshooting

See [rule troubleshooting](docs/Troubleshooting.md) for tips how to debug problems
in Minion rules.


## Project directory .minion

Whenever minion in launched, it creates `.minion` directory to the work directory.
The directory is used to store metadata about generated files.

The used rule file and source files are also stored and then used
as defaults on subsequent Minion launches. This means that you can drop rule file and
source codes from commands, and Minion will use the ones you provided earlier.

## Multiple rule-files in same work directory

You can use many rule-files in working directory, but you must then always provide the
`--rules` (or `-r`) option to specify which one you are using.

## Cleaning project

Command `minion clean` removes all generated output files from the working directory.
However, the directory `.minion` remains.
You can simply delete it manually after clean, 
if you want to totally remove or reset the project.

If you have many rule files, you must specify which one to clean or `clean -f`
to clean them all.

## Minion rules

As said, minion rule are stored into rule files with suffix `.rules`.
There is a set of built-in rule files, and you can create your own new ones.

A rule is loosely similar to a rule in Makefile and *process* in Data Flow Diagram (DFD).
A rule reads an *input file* and writes an *output file*.
Input and output files are similar to *prerequisities* and *targets* in a Makefile
and *data stores* in DFD.
Input and output files are *named* and these names are used to match
files together.
Optionally a rule can have a *predicate* which may bring in additional input files
and specify conditions which must be met for the rule to apply.

### Rule general form

A minion rule has the general format:

    rule <input> [<options>] [with <predicate>] => <output> [<options>]
        <command-1>
        <command-2>
        ...
        <command-n>

The commands are Shell script snipped executed as the rule is applied.
Inside the commands, variables of the form `$<name>` or `$(<name>)` are replaced by the input file
and output file names.
Special variable `$OUT`, or `$(OUT)`, is replaced by the output file name.

A rule can be configured with a set of options.
Options can be placed either after input file name or output file name,
this can be freely chosen.
There are examples of rule options below.

Consider the following simple rule:

    rule IN => type
        file -b --mime-type $IN > $OUT

The rule processes from input file "IN" into output file "type".
The special state "IN" stands for each input file given to minion from command line.
(Processed in parallel if multiple are given.)
Minion creates the rule output by running the command `file ...`.

The output from the rule is available for subsequence rules as input with a name "type".
Later you see examples how variables can be used in the subsequent rules.

If we assume input file is "`sample.txt`" and output file "`sample.txt.d/type`",
evaluation of the rule consist of running the following command:

    file -b --mime-type sample.txt > sample.txt.d/type

File name convention should follow the file naming convention of problem domain.
A file name must start with a letter or '\_' and after first character also digits and
characters '-' and '.' are allowed. It may be logical to reflect the output type of the
rule in the output file name suffix, e.g. "xyz.json" for rule producing JSON data.

You can also omit the command part, which makes the rule simply an alias
for the input data.

### Piping

If you do not specify the output variable
then output is taken from the *stdout* of the last command.
Using the implicit output you can shorten the above rule into this.

    rule IN => type
        file -b --mime-type $IN

You can also pipe data through *stdin*.
This is done by specifying the input file(s) followed by pipe '`|`'.
After the pipe specify the command taking input from stdin. For example:

    rule IN with lines.txt => line-count
        $(lines.txt) | wc -l

Use of stdin is required if there are *very* a lot of files to process, as resulting command line
length may exceed the maximum value allowed in the shell.

### Rule predicate

A rule predicate has two purposes.
Firstly, it can be used to filter which input files are processed by the rule.
Secondly, it allows additional input files for the rule.
A predicate is defined after rule start state with keyword `with`.

    rule IN with type == "application/pdf" => pdf
        cincan run cincan/pdfid $IN -j $OUT

The rule is only applied when file "type" has content "`application/pdf`".
File type can be resolved by an another rule as was shown earlier.

Special variable "name" holds the name of input file to use file name in condition.
However, there is a 'quick' format to use:
The following pattern can be used to apply to a file with a particular name:

    rule IN == "afile.txt" => afile

And the following format to use glob-matching to apply a rule:

    rule IN // "*.txt" => txt-file

### Command variables

Input file name and the additional input files from predicate are available for commands.
For example the following prints file and type for all files
(provided that "type" rule above is in effect):

    rule IN with type => type_and_file
        echo "$IN: $type"

Note how we only used 'type' without any conditionals etc. to just facilitate
its use in the command part. We also did not provide `$OUT` as rule output
can be read stdout.

As default, a variable holds the name of the file. The value of the
file is available using form `$value(name)`
For example, consider the following from PDF pipeline:

    rule docx with sha256 and clamav => docx-info.json
        jq -n '{"name": "$docx", "sha256": "$value(sha256)", "clamav": "$value(clamav)" }'

The rule is applied to output of rule `docx`, but it also requires output from rules `sha256`, and `clamav`. 

When there are multiple files for a variable used in predicate, 
the files are merged into single value by appending a space between the file names
(or values when using `$value(...)`)`.

### Defining values command-line

It is possible to define values directly in command line.
This can be used to define, e.g. passwords and API keys which should not be hardcoded
or stored to files.

Consider the following very simple rule:

    rule () with message => print
         echo $message

It is expected to define `message` in command line with `--define` or just `-D`.
For example, if the above rule is in "say.rules":

    minion -D"message=Hello, world" use say.rules

### Output directory

A rule can produce many output files by creating an output directory and filling it with files,
and perhaps sub-directories with files. E.g. consider the following rule

    rule IN with type == "application/zip" => IN
        unzip -d $OUT $IN

This rule inputs a zip-file and unpacks it into output directory.
This rule also uses the same name "IN" for input and output files.
As you may remember, "IN" initially holds the sames of Minion input files.
This rule adds files unpacked from zip files to this set of input files.
Would there be nested zip-files, this rule could recursive unpack also them.

### Input directory

Usually a rule inputs a file. Consider the following pair of rules:

    rule IN // "*.zip" => files
        unzip -d $OUT $IN

    rule files => echo-file
        echo $IN

The output "echo-file" is separately created to all input files unpacked from zip file.
Using rule option `--dir` a rule can be applied to directories rather than individual files, e.g.

    rule files --dir => echo-dir
        echo $IN

The output "echo-dir" is created only once for a directory.
Here we have placed the option `--dir ` immediately after input name for clarity
(it could as well as last thing of the rule first line).

An input file in predicate is not expanded to files, but it is the
directory if the corresponding output rule has produced a directory.
For example, the following

    rule IN with files => echo-pre
        echo $files

The variable "files" holds the name of the directory rather than many files.

In order to apply a rule both to directories and to input files,
one can take advantage of the following rule pattern:

    rule IN --dir => IN

This expands all input directories to input files.

## Collecting rules

Rules we have used this far, expand input files into one or many other output files.
The following rule does the opposite and collects many input rules into
single output.

    rule all-input <= IN
        cat $IN

Rule output target is give in left, followed by `<=` and trailed by the predicate.

FIXME: Example of predicate with collecting rule.
Example of predicate filtering the collected input files.

## Rule scopes

Rule *scope* is the set of names which are outputted, directly or through intermediate rules,
from the input file. This is best illustrated by this pair of rules:

    rule IN => type
        file -b --mime-type $IN

    rule IN with type == "application/zip" => IN
        unzip $IN -d $OUT

The first rule outputs file "type" to "IN" scope and the latter rule uses this
value to determine if "IN" is a file to unzip.
The possible unzipped files each form a new "IN" scope with "type" and thus
types do not conflict with each other.

If a name is not defined in the scope, then the value is searched outwards to other scopes until
it is found.

Non-default scope name can be defined explicitly, when the default scope is not good.
Scope can only be used for variables in the predicate.
Special scope name "GLOBAL" fetches all values, e.g.

    rule IN with GLOBAL:type => all-types
        cat $type

This would list all values of "type", not just for the input file in scope ("IN").

### Indexed file access

When a predicate input file is a directory, one can pick a single file from it 
using *indexed* file access. 
Consider the following:

    rule process with ps_names/[p_pid] => p_name
        cat $(ps_names)

Above rule fetches the "ps_name" file as indexed by value "p_pid".
E.g. if "p_pid" value is "`1234`", the value of "ps_names" in the command
will be "`ps_names/1234`".

Index can be more complex, e.g.

    rule IN with source/file-[index].tmp => indexed.value

This uses the value of rule "index" to pick one value from rule "source".
E.g. if index is "`xyz`", then picked value is "`source/file-xyz.tmp`".

### Batch rules

A *batch* rule can be used to process all input files into output files at once.
The batch rule is expected to produce one output file per each input file.
Minion moves the output files after processing to same locations as they would have been created by
normal rules.
Batch rules are marked with option `--batch`.

Consider the following trivial example, which only echoes the name of the file as rule output.

    rule IN => echo_me --batch
        mkdir -p $OUT
        for i in $IN; do (mkdir -p `dirname $OUT/$i` && echo $i > $OUT/$i); done

Note how the batch rule re-creates the input file structure under the "$OUT" directory.

Batch rules can be used when *all* input files must be processed at once and not by many calls to the rule.
For example, a virus scanning may have long set up time and thus running the scanner for each input
file would be very slow.

## File naming

Minion tries to be liberal with the names of the processed files. However, files starting with dot `.` and
files ending with `.d` are not allowed as data files. Also, files cannot have whitespace in their names.

Source files give to Minion cause an error if they are not conformant.
Files produced by rules are renamed, if they are not conforming with the rules above.

Note, that as output files are renamed, you can provide any input to Minion if you
put it inside an archive file (e.g. tar or zip) and provide a rule to unpack the archive.

## Default targets

The aforementioned 'use' sub command creates all *default target* files of the used rule set
(you can change the targets with --target <name> command line option).

A default target file is marked by rule option `--target`, which tells that the rule output file 
is a default target of the rule set.

The default targets should be fairly quick to process, so that by default invoking Minion
does not take too long and bore the use.
Use can then use other commands to create other output files beside the target.

## Minion sub commands

The minion sub-commands are the following.

| Sub command                       | Description             |
|-----------------------------------|-------------------------|
| **use** files... (*               | Use a specific rule set to process files |
| **build** files... (*             | Build all possible output files |
| **get** target files... (*        | Get the value of the specific target |
| **file** path files... (*         | Get a file by its path |
| **explain** file                  | Explain how the file was created |
| **export** format rules           | Export rules eg. to activity diagram file |
| **clean**                         | Clean all Minion-generated files |
| **ls**                            | List input and output files |
| **help**                          | Get command-line help |

*) You can omit 'rules' and 'files' if default ones have been stored.

Sub-command specific help is available with `--help` after the sub-command, e.g.
`minion use --help`

The following options are applicable for all sub commands, and must be specified before
the sub command.

| Option                |     | Description             |
|-----------------------|-----|-------------------------|
| --rules **rules-file** | -r  | Use other rule file than the default
| --content             | -c  | Return file content, not files, when applicable
| --log **level**       | -l  | Change logging level (see --help for a list)
| --quiet               | -q  | Be quiet, return only result etc.
| --threads N           |     | Use max. of N threads for processing
| --rule-timeout N      | -t  | Rule timeout in seconds, defaults to 300 (5 mins)
| --live-log            |     | Show "live log" of rules
| --define              | -D  | Define value in form "name=value"
| --ignore-errors       |     | Tell Minion not to rerun rules due changes
