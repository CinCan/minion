from minion import start
from tests.test_context import establish_directory


def test_export(capsys):
    test_dir = establish_directory('context')
    start.main(['-C', test_dir.as_string(), '-r', 'tests/rules/copy.rules', 'export', 'json'])
    stdout = capsys.readouterr().out
    assert '{"nodes": [{"id": "IN"}, {"id": "file.copy"}]' in stdout
