from minion.file_facade import Facade
from minion.value import ValueName
from tests.test_context import establish_directory


def test_facade_with_copy():
    test_dir = establish_directory('context', rule_file='copy', sources='txt_2')
    facade = Facade.new(test_dir, sources=[test_dir / 'a.txt', test_dir / 'b.txt'])
    f = facade.get_data_file(test_dir / 'a.txt')
    assert f.value() == 'This is text A'
    f = facade.get_data_file(test_dir / 'b.txt')
    assert f.value() == 'This is text B'

    assert not (test_dir / '.minion').exists()  # not created, yet

    f = facade.get_data_file(test_dir / 'b.txt.d' / 'file.copy')
    assert (test_dir / '.minion').exists()
    assert f.value() == 'This is text B'
    assert test_dir / 'a.txt.d' / 'file.copy' not in facade.scheduler.contexts
    assert test_dir / 'b.txt.d' / 'file.copy' in facade.scheduler.contexts


def test_facade_with_existing_files():
    test_dir = establish_directory('context', rule_file='copy', sources='txt_2')
    facade = Facade.new(test_dir, sources=[test_dir / 'a.txt', test_dir / 'b.txt'])
    f = facade.get_context(test_dir / 'a.txt')
    assert not f.created
    f = facade.get_context(test_dir / 'a.txt.d' / 'file.copy')
    assert f.created

    facade = Facade.new(test_dir, sources=[test_dir / 'a.txt', test_dir / 'b.txt'])
    f = facade.get_context(test_dir / 'a.txt')
    assert not f.created
    f = facade.get_context(test_dir / 'a.txt.d' / 'file.copy')
    assert not f.created

    facade = Facade.new(test_dir, sources=[test_dir / 'a.txt', test_dir / 'b.txt'])
    (test_dir / 'a.txt').path.touch(exist_ok=True)
    f = facade.get_context(test_dir / 'a.txt')
    assert not f.created
    f = facade.get_context(test_dir / 'a.txt.d' / 'file.copy')
    assert f.created

    facade = Facade.new(test_dir, sources=[test_dir / 'a.txt', test_dir / 'b.txt'])
    f = facade.get_context(test_dir / 'a.txt')
    assert not f.created
    f = facade.get_context(test_dir / 'a.txt.d' / 'file.copy')
    assert not f.created


def test_facade_with_unzip():
    test_dir = establish_directory('context', rule_file='type_strings_unzip', sources='zip_txt')
    facade = Facade.new(test_dir, sources=[test_dir / 'txt.zip'])

    f = facade.get_data_file(test_dir / 'txt.zip.d' / 'IN')
    assert f.is_dir() and f.name() == 'IN'

    f = facade.get_data_file(test_dir / 'txt.zip.d' / 'IN' / 'b.txt')
    assert f.value() == 'This is text B'


def test_facade_build():
    test_dir = establish_directory('context', rule_file='type_strings_unzip', sources='zip_txt')
    facade = Facade.new(test_dir, sources=[test_dir / 'txt.zip'])

    facade.build_all()
    f = sorted(facade.manager.root_file.list_files(recurse=True))
    assert f[0].as_string() == 'minion.rules'
    assert f[1].as_string() == 'txt.zip'
    assert f[9].as_string() == 'txt.zip.d/type'
    assert len(f) == 10


def test_facade_with_multivalue():
    test_dir = establish_directory('context', rule_file='multivalue_variable', sources='zip_zip_txt')
    facade = Facade.new(test_dir, sources=[test_dir / 'txt.zip'])

    f = facade.get_data_file(test_dir / 'txt.zip.d' / 'digests')
    parts = sorted(f.value().split(' '))
    assert parts == ['txt.zip.d/file/a.txt.d/md', 'txt.zip.d/file/b.txt.d/md',
                     'txt.zip.d/file/sub.zip.d/file/c.txt.d/md', 'txt.zip.d/file/sub.zip.d/file/d.txt.d/md']


def test_find_value_with_multivalue():
    test_dir = establish_directory('context', rule_file='multivalue_variable', sources='zip_zip_txt')
    facade = Facade.new(test_dir, sources=[test_dir / 'txt.zip'])

    fs = facade.find_values(ValueName('md'))
    assert fs[0].as_string() == 'txt.zip.d/file/a.txt.d/md'
    assert fs[1].as_string() == 'txt.zip.d/file/b.txt.d/md'
    assert fs[2].as_string() == 'txt.zip.d/file/sub.zip.d/file/c.txt.d/md'
    assert fs[3].as_string() == 'txt.zip.d/file/sub.zip.d/file/d.txt.d/md'
    assert len(fs) == 4


def test_multivalue_existing_files():
    test_dir = establish_directory('context', rule_file='multivalue_variable', sources='zip_zip_txt')
    facade = Facade.new(test_dir, sources=[test_dir / 'txt.zip'])
    fs = facade.find_contexts(ValueName('md'))
    assert (facade / 'txt.zip' / 'file').created
    assert all([f.created for f in fs])
    assert len(fs) == 4

    facade = Facade.new(test_dir, sources=[test_dir / 'txt.zip'])
    fs = facade.find_contexts(ValueName('md'))
    assert not (facade / 'txt.zip' / 'file').created
    assert all([not f.created for f in fs])
    assert len(fs) == 4


def test_facade_with_batch():
    test_dir = establish_directory('context', rule_file='multivalue_with_batch', sources='zip_zip_txt')
    facade = Facade.new(test_dir, sources=[test_dir / 'txt.zip'])

    f = facade.get_data_file(test_dir / 'txt.zip.d' / 'file' / 'a.txt.d' / 'md')
    assert f.value() == 'cfb1b4571285a792252cb8a3d862a71a  txt.zip.d/file/a.txt'


def test_facade_with_batch_get():
    test_dir = establish_directory('context', rule_file='multivalue_with_batch', sources='zip_zip_txt')
    facade = Facade.new(test_dir, sources=[test_dir / 'txt.zip'])
    f = facade.find_values(ValueName('md'))
    assert len(f) == 4

    facade = Facade.new(test_dir, sources=[test_dir / 'txt.zip'])
    f = facade.find_values(ValueName('md'))
    assert len(f) == 4


def test_facade_with_type_unzip_batch():
    test_dir = establish_directory('context', rule_file='type_unzip_with_batch', sources='zip_zip_txt')
    facade = Facade.new(test_dir, sources=[test_dir / 'txt.zip'])
    f = facade.find_values(ValueName('strings'))
    assert len(f) == 6

    facade = Facade.new(test_dir, sources=[test_dir / 'txt.zip'])
    f = facade.find_values(ValueName('strings'))
    assert len(f) == 6


def test_facade_with_type_unzip_batch_get():
    test_dir = establish_directory('context', rule_file='type_unzip_with_batch', sources='zip_zip_txt')
    facade = Facade.new(test_dir, sources=[test_dir / 'txt.zip'])

    f = facade.find_values(ValueName('md'))
    assert len(f) == 4


def test_batch_existing_files():
    test_dir = establish_directory('context', rule_file='multivalue_with_batch', sources='zip_zip_txt')
    facade = Facade.new(test_dir, sources=[test_dir / 'txt.zip'])
    f = facade.get_context(test_dir / 'txt.zip.d' / 'file' / 'a.txt.d' / 'md')
    assert f.created

    facade = Facade.new(test_dir, sources=[test_dir / 'txt.zip'])
    f = facade.get_context(test_dir / 'txt.zip.d' / 'file' / 'a.txt.d' / 'md')
    assert not f.created

    (test_dir / 'txt.zip').path.touch(exist_ok=True)
    facade = Facade.new(test_dir, sources=[test_dir / 'txt.zip'])
    f = facade.get_context(test_dir / 'txt.zip.d' / 'file' / 'a.txt.d' / 'md')
    assert f.created

    facade = Facade.new(test_dir, sources=[test_dir / 'txt.zip'])
    f = facade.get_context(test_dir / 'txt.zip.d' / 'file' / 'a.txt.d' / 'md')
    assert not f.created


def test_root_rule_in_depth():
    test_dir = establish_directory('context', rule_file='root_rule_in_depth', sources='zip_zip_txt')
    facade = Facade.new(test_dir, sources=[test_dir / 'txt.zip'])
    f = facade.get_data_file(test_dir / 'all-text')
    assert f.value() == 'This is not text A\nThis is not text B\nThis is not text C\nThis is not text D'


def test_root_rule_find():
    test_dir = establish_directory('context', rule_file='root_rule_in_depth', sources='zip_zip_txt')
    facade = Facade.new(test_dir, sources=[test_dir / 'txt.zip'])
    f = facade.find_values(ValueName('all-text'))
    assert f[0].value() == 'This is not text A\nThis is not text B\nThis is not text C\nThis is not text D'
    assert len(f) == 1


def test_root_rule_build():
    test_dir = establish_directory('context', rule_file='root_rule_in_depth', sources='zip_zip_txt')
    facade = Facade.new(test_dir, sources=[test_dir / 'txt.zip'])
    facade.build_all()

    f = sorted(facade.manager.root_file.list_files(recurse=True))
    assert f[0].as_string() == 'all-text'
    assert f[1].as_string() == 'minion.rules'
    assert f[2].as_string() == 'txt.zip'
    assert f[17].as_string() == 'txt.zip.d/type'
    assert len(f) == 18
