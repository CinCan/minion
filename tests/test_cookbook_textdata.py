from tests.test_cookbook_basic import copy_from_cookbook


def test_find_line():
    out = copy_from_cookbook("textdata", "find-line.sh")
    print(out)
    assert len(out.split('\n')) == 6


def test_print_column():
    out = copy_from_cookbook("textdata", "print-column.sh")
    print(out)
    assert out == '4\n252\n328\n364\n372\n400\n460\n468\n476\n584\n648\n700\n760\n868\n908\n'


def test_group_by_key():
    out = copy_from_cookbook("textdata", "group-by-key.sh")
    print(out)
    ofs = out.split('\n')
    ps = ofs[:10]
    assert ps[0] == 'pslist.txt.d/process-by-name/System'
    assert ps[9] == 'pslist.txt.d/process-by-name/winlogon.exe'
    cs = ofs[11:16]
    assert cs[0] == '>|0x84a995e0|svchost.exe|584|460|15|371|0|0|2019-10-28 15:47:32 UTC+0000|'
    assert cs[4] == '>|0x84b1c030|svchost.exe|908|460|24|376|0|0|2019-10-28 15:47:33 UTC+0000|'


def test_enrich_group_data():
    out = copy_from_cookbook("textdata", "enrich-group-data.sh")
    print(out)
    with open('tests/test_enrich_group_data.out') as f:
        assert out == f.read()


def test_index_data():
    out = copy_from_cookbook("textdata", "index-data.sh")
    print(out)
    with open('tests/test_index_data.out') as f:
        assert out == f.read()
