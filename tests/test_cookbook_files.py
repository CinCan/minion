from tests.test_cookbook_basic import copy_from_cookbook


def test_readme_and_dockerfile():
    out = copy_from_cookbook("files", "readme_and_dockerfile.sh")
    print(out)
    with open('tests/test_readme_and_dockerfile.out') as f:
        assert out == f.read()


def test_scan_subdirectories():
    out = copy_from_cookbook("files", "scan_subdirectories.sh")
    print(out)
    split_index = out.index('===')
    out1 = out[0:split_index]
    out2 = out[split_index + 4:]
    with open('tests/test_readme_and_dockerfile.out') as f:
        assert out1 == f.read()
    assert out1 == out2
