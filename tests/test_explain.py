from io import StringIO

import pytest

from minion.file_facade import Facade
from tests.test_context import establish_directory


def test_explain():
    test_dir = establish_directory('context', rule_file='root_rule_in_depth', sources='zip_zip_txt')
    facade = Facade.new(test_dir, sources=[test_dir / 'txt.zip'])
    facade.build_all()

    buf = StringIO()
    facade.manager.registry.explain(buf, test_dir / 'txt.zip.d' / 'type')
    assert buf.getvalue() == 'mkdir -p txt.zip.d\nfile -b --mime-type txt.zip > txt.zip.d/type\n'

    buf = StringIO()
    facade.manager.registry.explain(buf, test_dir / 'txt.zip.d' / 'IN' / 'sub.zip')
    assert buf.getvalue() == 'mkdir -p txt.zip.d\nfile -b --mime-type txt.zip > txt.zip.d/type\n' + \
        'mkdir -p txt.zip.d\nunzip txt.zip -d txt.zip.d/IN\n'


def test_explain_no_project():
    test_dir = establish_directory('context', rule_file='root_rule_in_depth', sources='zip_zip_txt')
    facade = Facade.new(test_dir, sources=[test_dir / 'txt.zip'])

    buf = StringIO()
    with pytest.raises(Exception) as e:
        facade.manager.registry.explain(buf, test_dir / 'txt.zip.d' / 'type')
    assert str(e.value).startswith("Could not locate .minion directory for")


def test_explain_with_batch():
    test_dir = establish_directory('context', rule_file='root_rule_and_batch_rule', sources='zip_txt')
    facade = Facade.new(test_dir, sources=[test_dir / 'txt.zip'])
    facade.build_all()

    buf = StringIO()
    facade.manager.registry.explain(buf, test_dir / 'txt.zip.d' / 'IN' / 'a.txt.d' / 'text')
    assert buf.getvalue() == 'mkdir -p txt.zip.d\n' +\
        'file -b --mime-type txt.zip > txt.zip.d/type\n' +\
        'mkdir -p txt.zip.d\n' +\
        'unzip txt.zip -d txt.zip.d/IN\n' +\
        'mkdir -p txt.zip.d/IN/a.txt.d\n' +\
        'file -b --mime-type txt.zip.d/IN/a.txt > txt.zip.d/IN/a.txt.d/type\n' +\
        'mkdir -p txt.zip.d/IN/a.txt.d\n' +\
        'sed "s/This is/This is not/" txt.zip.d/IN/a.txt > txt.zip.d/IN/a.txt.d/text\n'
