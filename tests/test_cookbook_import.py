from tests.test_cookbook_basic import copy_from_cookbook


def test_import_example():
    out = copy_from_cookbook("rule_import", "import_example.sh")
    print(out)
    assert out == "This is text A\nThis is text B\n"


def test_import_rename():
    out = copy_from_cookbook("rule_import", "import_rename.sh")
    print(out)
    assert out == "Type of sample.zip.d/IN/a.txt: text/plain\nType of sample.zip.d/IN/b.txt: text/plain\n" +\
        "Type of sample.zip: application/zip\n"
