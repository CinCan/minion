import pathlib

from minion.file import DataFile
from minion.rule_library import FileBasedRuleLibrary
from minion.value import ValueName, INPUT_VALUE, GLOBAL


def test_parse_sample_pdf_pipeline():
    lib = FileBasedRuleLibrary(DataFile(pathlib.Path() / 'minion' / 'rules' / 'samples' / 'pdf-pipeline.rules'))
    assert len(list(lib.get_rules(ValueName('IN')))) == 11
    assert len(list(lib.get_rules(ValueName('pdf')))) == 2
    assert len(list(lib.get_rules(ValueName('jsunpack')))) == 1
    assert len(list(lib.get_rules(ValueName('docx')))) == 1
    assert len(list(lib.get_rules(ValueName('GLOBAL')))) == 1

    rs = lib.get_rules_to_find(INPUT_VALUE, ValueName('type'))
    assert sorted([r.end.name for r in rs]) == ['IN', 'IN', 'IN', 'type']
    rs = lib.get_rules_to_find(INPUT_VALUE, ValueName('clamav'))
    assert sorted([r.end.name for r in rs]) == ['IN', 'IN', 'IN', 'clamav']
    rs = lib.get_rules_to_find(INPUT_VALUE, ValueName('summary.json'))
    assert sorted([r.end.name for r in rs]) == []
    rs = lib.get_rules_to_find(GLOBAL, ValueName('summary.json'))
    assert sorted([r.end.name for r in rs]) == ['summary.json']

    rs = lib.get_rules_to_find(INPUT_VALUE, ValueName('pdf.json'))
    assert sorted([r.end.name for r in rs]) == ['IN', 'IN', 'IN', 'pdf.json']
