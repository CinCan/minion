import json
import threading
import time
from typing import Dict, List

import requests
from requests import Request

from minion.file import DataFile
from minion.file_facade import Facade
from minion.remote_tracker import RemoteTracker
from tests.test_context import establish_directory


def start_api(test_dir: DataFile, sources: List[DataFile] = None) -> RemoteTracker:
    facade = Facade.new(test_dir, sources=sources)
    tracker = RemoteTracker.attach(facade, None, 'hophip')

    # start facade to allow test thread to connect
    def run_facade():
        facade.start()
        facade.build_all()
        facade.stop()
    runner = threading.Thread(target=run_facade)
    runner.daemon = True
    runner.start()
    time.sleep(0.1)

    return tracker


def make_request(track: RemoteTracker, method: str, path: str) -> Dict:
    req = Request(method, f'http://{track.local_addr}:{track.local_port}/{path}',
                  headers={'X-API-Key': track.api_key}).prepare()
    r = requests.session().send(req)
    if r.status_code != 200:
        raise Exception(f"{r.status_code} {r.reason}")
    if not r.content:
        return {}
    rc = json.loads(r.content)
    return rc


def test_pause_all():
    test_dir = establish_directory('context', rule_file='copy', sources='txt_3')
    track = start_api(test_dir, sources=[test_dir / 'a.txt', test_dir / 'b.txt', test_dir / 'c.txt'])
    rc = make_request(track, 'GET', 'events/latest?timeout=1000')
    assert rc['event'] == 0
    assert 'start' in rc
    rc = make_request(track, 'PUT', 'events?action=continue')
    assert not rc

    rc = make_request(track, 'GET', 'events/1?timeout=1000')
    assert rc['event'] == 1
    assert rc['context']['start'] == 'IN'
    assert rc['context']['end'] == 'file.copy'
    assert 'start' not in rc
    rc = make_request(track, 'PUT', 'events?action=continue')
    assert not rc

    rc = make_request(track, 'GET', 'events/2?timeout=1000')
    assert rc['event'] == 2
    rc = make_request(track, 'PUT', 'events?action=continue')
    assert not rc

    rc = make_request(track, 'GET', 'events/3?timeout=1000')
    assert rc['event'] == 3
    assert 'stop' not in rc
    rc = make_request(track, 'PUT', 'events/3?action=continue')  # specify event num!
    assert not rc

    rc = make_request(track, 'GET', 'events/4?timeout=1000')
    assert rc['event'] == 4
    assert 'stop' in rc
    rc = make_request(track, 'PUT', 'events?action=continue')
    assert not rc

    track.wait_to_stop()


def test_run_all():
    test_dir = establish_directory('context', rule_file='copy', sources='txt_3')
    track = start_api(test_dir, sources=[test_dir / 'a.txt', test_dir / 'b.txt', test_dir / 'c.txt'])
    rc = make_request(track, 'GET', 'events/0?timeout=1000')
    assert rc['event'] == 0
    assert 'start' in rc
    rc = make_request(track, 'PUT', 'pause?value=false')
    assert not rc
    rc = make_request(track, 'PUT', 'events?action=continue')
    assert not rc

    track.wait_to_stop()


def test_breakpoint():
    test_dir = establish_directory('context', rule_file='copy', sources='txt_3')
    track = start_api(test_dir, sources=[test_dir / 'a.txt', test_dir / 'b.txt', test_dir / 'c.txt'])
    rc = make_request(track, 'GET', 'events/latest?timeout=1000')
    assert rc['event'] == 0
    assert 'start' in rc
    rc = make_request(track, 'POST', f'breakpoints/{test_dir.path.absolute().as_posix()}/minion.rules?line=1')
    assert not rc
    rc = make_request(track, 'PUT', 'events?action=continue&pause=false')
    assert not rc

    rc = make_request(track, 'GET', 'events/1?timeout=500')
    assert rc['event'] == 1
    assert rc['context']['start'] == 'IN'
    assert rc['context']['end'] == 'file.copy'
    assert 'start' not in rc

    # Test listing of events
    rc = make_request(track, 'GET', 'events')
    assert len(rc['events']) == 1

    rc = make_request(track, 'DELETE', f'breakpoints/{test_dir.path.absolute().as_posix()}/minion.rules?line=1')
    assert not rc
    rc = make_request(track, 'PUT', 'events?action=continue')
    assert not rc

    track.wait_to_stop()


def test_context_info():
    test_dir = establish_directory('context', rule_file='collect', sources='txt_3')
    track = start_api(test_dir, sources=[test_dir / 'a.txt', test_dir / 'b.txt', test_dir / 'c.txt'])
    rc = make_request(track, 'GET', 'events/latest?timeout=1000')
    assert rc['event'] == 0
    assert 'start' in rc
    rc = make_request(track, 'POST', f'breakpoints/{test_dir.path.absolute().as_posix()}/minion.rules?line=1')
    assert not rc
    rc = make_request(track, 'PUT', 'events?action=continue&pause=false')
    assert not rc

    rc = make_request(track, 'GET', 'events/1?timeout=500')
    assert rc['event'] == 1
    assert rc['context']['path'] == 'print'

    rc = make_request(track, 'GET', f'contexts/{test_dir.path.as_posix()}/print')
    assert rc['path'] == 'print'
    assert rc['values'][0]['name'] == 'IN'
    assert rc['values'][0]['paths'] == ['a.txt', 'c.txt']
    assert rc['values'][1]['name'] == 'tag'
    assert rc['values'][1]['paths'] == ['a.txt.d/tag', 'c.txt.d/tag']

    rc = make_request(track, 'DELETE', f'breakpoints/{test_dir.path.absolute().as_posix()}/minion.rules?line=1')
    assert not rc
    rc = make_request(track, 'PUT', 'events?action=continue')
    assert not rc

    track.wait_to_stop()
