import json
import pathlib

import pytest

from minion import start
from tests.test_context import establish_directory


def test_basic_use(capsys):
    test_dir = establish_directory('context', sources='txt_1')
    start.main(['-C', test_dir.path.as_posix(), '-r', 'tests/rules/copy.rules', 'use', 'tests/_context/a.txt'])
    stdout = capsys.readouterr().out
    f = test_dir / 'a.txt.d' / 'file.copy'
    assert f.value() == 'This is text'
    # pytest set logging level to WARNING
    # assert 'cp -a a.txt a.txt.d/file.copy' in stdout
    # assert 'a.txt.d/file.copy' in stdout

    with (test_dir.path / '.minion' / 'copy.rules' / 'settings').open("r") as f:
        settings = json.load(f)
    assert settings['sources'] == ["tests/_context/a.txt"]

    start.main(['-C', test_dir.path.as_posix(), '-r', 'tests/rules/copy.rules', 'use', '-t', 'file.copy'])
    stdout = capsys.readouterr().out
    assert 'a.txt.d/file.copy' == stdout.split('\n')[0]


def test_bad_target_use(capsys):
    test_dir = establish_directory('context', sources='txt_1')
    with pytest.raises(Exception) as e:
        start.main(['-C', test_dir.path.as_posix(), '-r', 'tests/rules/copy.rules', 'use', '-t', 'no-such',
                    'tests/_context/a.txt'])
    assert str(e.value) == "Unknown target 'no-such'"


def test_no_rules_use(capsys):
    test_dir = establish_directory('context', sources='txt_1')
    with pytest.raises(Exception) as e:
        start.main(['-C', test_dir.path.as_posix(), '-r', 'tests/rules/empty.rules', 'use', 'tests/_context/a.txt'])
    assert str(e.value) == "No rules to apply"


def test_no_input_file_match(capsys):
    test_dir = establish_directory('context', sources='txt_1')
    with pytest.raises(Exception) as e:
        start.main(['-C', test_dir.path.as_posix(), '-r', 'tests/rules/only_print_dirs.rules', 'use',
                    'tests/_context/a.txt'])
    assert str(e.value) == "No input files matched any rules"


def test_quiet(capsys):
    test_dir = establish_directory('context', sources='txt_1')
    start.main(['-C', test_dir.path.as_posix(), '-q', '-r', 'tests/rules/copy.rules', 'use', 'tests/_context/a.txt'])
    stdout = capsys.readouterr().out
    assert 'a.txt.d/file.copy' == stdout.split('\n')[1]


def test_no_arguments_no_defaults(capsys):
    test_dir = establish_directory('context')
    start.main([])
    stdout = capsys.readouterr().out
    assert "usage:" in stdout


def test_no_files(capsys):
    test_dir = establish_directory('context', sources='txt_1')
    with pytest.raises(Exception) as e:
        assert start.main(['-C', test_dir.as_string(), '-r', 'unpack.rules', 'use'])
    assert str(e.value) == "No input files provided (value 'IN' used in rules)"


def test_list_rules(capsys):
    test_dir = establish_directory('context', sources='txt_1')
    start.main(['-C', test_dir.path.as_posix(), 'use'])
    stderr = capsys.readouterr().err
    assert stderr == 'No rule set specified, built-in rule sets (choose by --rules <name> use):\n' \
        + 'hello.rules\n' \
        + 'unpack.rules\n' \
        + 'samples/volatility.rules\n' \
        + 'samples/scrape-website.rules\n' \
        + 'samples/pdf-pipeline.rules\n'


def test_clean(capsys):
    test_dir = establish_directory('context', sources='txt_1')
    start.main(['-C', test_dir.path.as_posix(), '-r', 'tests/rules/copy.rules', 'use', '-t', 'file.copy',
                'tests/_context/a.txt'])
    f = test_dir / 'a.txt.d' / 'file.copy'
    assert f.value() == 'This is text'

    start.main(['-C', test_dir.path.as_posix(), '-r', 'tests/rules/copy.rules', 'clean'])
    assert (test_dir / 'a.txt').exists()
    assert not (test_dir / 'a.txt.d' / 'file.copy').exists()
    assert not (test_dir / '.minion/meta').exists()


def test_clean_no_minion(capsys):
    test_dir = establish_directory('context', mkdir=False)
    with pytest.raises(Exception):
        start.main(['-C', test_dir.path.as_posix(), 'clean'])
