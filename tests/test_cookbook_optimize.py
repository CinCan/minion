from tests.test_cookbook_basic import copy_from_cookbook


def test_stdin_pipe():
    out = copy_from_cookbook("optimize", "stdin-pipe.sh")
    print(out)
    c = sorted(out.strip().split('\n'))
    assert c[:4] == ['File A', 'File B', 'File C', 'File D']


def test_batch():
    out = copy_from_cookbook("optimize", "batch.sh")
    print(out)
    with open('tests/test_batch.out') as f:
        assert out == f.read()
