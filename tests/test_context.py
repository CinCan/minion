import pathlib
import shutil
from typing import Dict

from minion.builder import Builder
from minion.predicate import Predicates
from minion.context import FileState
from minion.interfaces import DataFile
from minion.rules import CommandRule
from minion.value import ValueName, INPUT_VALUE


def establish_directory(name: str, mkdir: bool = True, rule_file: str = None, sources: str = None,
                        files: Dict[str, str] = None) -> DataFile:
    root_dir = pathlib.Path(__file__).parent / ("_" + name)
    shutil.rmtree(root_dir, ignore_errors=True)
    print(f"Test directory {root_dir.as_posix()}")
    if mkdir:
        root_dir.mkdir(parents=True, exist_ok=True)
    if rule_file is not None:
        rule_src_file = pathlib.Path(__file__).parent / "rules" / (rule_file + '.rules')
        shutil.copy(rule_src_file, root_dir / 'minion.rules')
    if sources is not None:
        sample_sources_dir = pathlib.Path(__file__).parent / "samples" / sources
        for s_file in sample_sources_dir.iterdir():
            if s_file.is_dir():
                shutil.copytree(s_file, root_dir / s_file.name)
            else:
                shutil.copy(s_file, root_dir / s_file.name)
    if files:
        for name, content in files.items():
            file = root_dir / name
            with file.open('w') as f:
                f.write(content)
    return DataFile.new(root_dir)


def test_context_basics():
    a_rule = CommandRule.new(
        INPUT_VALUE, 'a_rule', commands=['cat $IN'])
    test_dir = establish_directory('context', files={'a_file': ''})
    builder = Builder.new(test_dir, rules=[a_rule], sources=[test_dir / 'a_file'])

    a_links = builder.get_value(builder.contexts[0], ValueName('a_rule'))
    assert len(a_links) == 1
    assert a_links[0].name == ValueName('a_rule')
    assert a_links[0].file.as_string() == 'a_file.d/a_rule'

    pre = builder.get_to_evaluate()
    assert not builder.prepared
    pre.apply(capture_errors=False)
    meta = builder.registry.get_metadata(builder.root_file / 'a_file.d' / 'a_rule')
    assert len(meta.input) == 1
    assert meta.input[0].path == 'a_file'
    assert meta.input[0].digest == 'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855'
    assert len(meta.output) == 1
    assert meta.output[0].path == 'a_file.d/a_rule'
    assert meta.output[0].digest == 'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855'


def test_context_requirements():
    type_rule = CommandRule.new(
        INPUT_VALUE, 'type', commands=['file -b --mime-type $IN'])
    copy_rule = CommandRule.new(
        INPUT_VALUE, 'copy', commands=['cp $IN $OUT'], predicate=Predicates.ref('type'))
    test_dir = establish_directory('context', files={'a_file': 'This is text'})
    builder = Builder.new(test_dir, rules=[copy_rule, type_rule], sources=[test_dir / 'a_file'])

    a_links = builder.get_value(builder.contexts[0], ValueName('copy'))
    assert len(a_links) == 1
    assert a_links[0].name == ValueName('copy')
    assert len(builder.prepared) == 2
    assert builder.prepared[0].name() == ValueName('type')
    assert builder.prepared[1].name() == ValueName('copy')

    pre = builder.get_to_evaluate()
    assert pre.name() == ValueName('type')
    pre_not = builder.get_to_evaluate()
    assert pre_not is None
    pre.apply(capture_errors=False)
    assert pre.context.file.value() == 'text/plain'
    builder.post_process(pre)

    pre = builder.get_to_evaluate()
    assert pre.name() == ValueName('copy')
    pre_not = builder.get_to_evaluate()
    assert pre_not is None

    pre.apply(capture_errors=False)
    assert pre.context.file.value() == 'This is text'


def test_context_requirements_2():
    type_rule = CommandRule.new(
        INPUT_VALUE, 'type', commands=['file -b --mime-type $IN'])
    echo_rule = CommandRule.new(
        INPUT_VALUE, 'echo', commands=['echo $md'], predicate=Predicates.ref('md'))
    md_rule = CommandRule.new(
        'type', 'md', commands=['cat $type | md5sum'])
    test_dir = establish_directory('context', files={'a_file': 'This is text'})
    builder = Builder.new(test_dir, rules=[type_rule, echo_rule, md_rule], sources=[test_dir / 'a_file'])

    builder.get_value(builder.contexts[0], ValueName('echo'))

    pre = builder.get_to_evaluate()
    assert pre.name() == ValueName('type')
    pre_not = builder.get_to_evaluate()
    assert pre_not is None
    pre.apply(capture_errors=False)
    assert pre.context.file.value() == 'text/plain'
    builder.post_process(pre)

    pre = builder.get_to_evaluate()
    assert pre.name() == ValueName('md')
    pre_not = builder.get_to_evaluate()
    assert pre_not is None
    pre.apply(capture_errors=False)
    assert pre.context.file.value() == '25b22c96abec60203a2fc62b2cddfb67  -'
    builder.post_process(pre)

    pre = builder.get_to_evaluate()
    assert pre.name() == ValueName('echo')
    pre_not = builder.get_to_evaluate()
    assert pre_not is None
    pre.apply(capture_errors=False)
    assert pre.context.file.value() == 'a_file.d/type.d/md'


def test_value_in_many_branches():
    a_rule = CommandRule.new(
        INPUT_VALUE, 'a', commands=['echo A'])
    b_rule = CommandRule.new(
        INPUT_VALUE, 'b', commands=['echo B'])
    c_rule = CommandRule.new(
        INPUT_VALUE, 'c', commands=['echo C'])
    d_rule = CommandRule.new(
        ValueName('c'), 'd', commands=['echo D'])
    sub_rules = \
        [CommandRule.new(r.end, 'sub', commands=[f"echo .`cat ${r.end}`."]) for r in [a_rule, b_rule, d_rule]]
    sum_rule = CommandRule.new(
        INPUT_VALUE, 'sum', commands=['echo $sub'], predicate=Predicates.ref('IN:sub'))
    test_dir = establish_directory('context', files={'a_file': ''})  # FIXME: Get rid of artificial root file!
    builder = Builder.new(test_dir, rules=[a_rule, b_rule, c_rule, d_rule, sum_rule] + sub_rules,
                          sources=[test_dir / 'a_file'])

    builder.get_value(builder.contexts[0], ValueName('sum'))
    pre = builder.get_to_evaluate().apply(capture_errors=False)
    assert pre.path_string() == 'a_file.d/c'
    assert pre.context.file.value() == 'C'
    builder.post_process(pre)
    pre = builder.get_to_evaluate().apply(capture_errors=False)
    assert pre.path_string() == 'a_file.d/c.d/d'
    assert pre.context.file.value() == 'D'
    builder.post_process(pre)
    pre = builder.get_to_evaluate().apply(capture_errors=False)
    assert pre.path_string() == 'a_file.d/c.d/d.d/sub'
    assert pre.context.file.value() == '.D.'
    builder.post_process(pre)
    pre = builder.get_to_evaluate().apply(capture_errors=False)
    assert pre.path_string() == 'a_file.d/b'
    assert pre.context.file.value() == 'B'
    builder.post_process(pre)
    pre = builder.get_to_evaluate().apply(capture_errors=False)
    assert pre.path_string() == 'a_file.d/b.d/sub'
    assert pre.context.file.value() == '.B.'
    builder.post_process(pre)
    pre = builder.get_to_evaluate().apply(capture_errors=False)
    assert pre.path_string() == 'a_file.d/a'
    assert pre.context.file.value() == 'A'
    builder.post_process(pre)
    pre = builder.get_to_evaluate().apply(capture_errors=False)
    assert pre.path_string() == 'a_file.d/a.d/sub'
    assert pre.context.file.value() == '.A.'
    builder.post_process(pre)
    pre = builder.get_to_evaluate().apply(capture_errors=False)
    assert pre.path_string() == 'a_file.d/sum'
    assert pre.context.file.value() == 'a_file.d/a.d/sub a_file.d/b.d/sub a_file.d/c.d/d.d/sub'
    builder.post_process(pre)


def test_multi_file_output():
    unzip_rule = CommandRule.new(INPUT_VALUE, 'unzip', commands=['unzip -d $OUT $IN'])
    a_rule = CommandRule.new('unzip', 'a', commands=['cat $unzip'])
    test_dir = establish_directory('context', sources='zip_txt')
    builder = Builder.new(test_dir, rules=[unzip_rule, a_rule], sources=[test_dir / 'txt.zip'])

    builder.get_value(builder.contexts[0], ValueName('unzip'))
    pre = builder.get_to_evaluate().apply(capture_errors=False)
    assert pre.path_string() == 'txt.zip.d/unzip'
    builder.post_process(pre)
    unzip = builder.contexts[0] / 'unzip'
    assert unzip.directory_of

    builder.get_value(unzip, ValueName('a'))
    pre = builder.get_to_evaluate().apply(capture_errors=False)
    assert pre.context.state == FileState.EVALUATE
    assert pre.context.file.as_string() == 'txt.zip.d/unzip/b.txt.d/a'
    builder.post_process(pre)
    files = list(unzip.outputs[ValueName('*')].iterate_contexts())
    assert len(files) == 2
    a, b = files
    assert a.file.value() == 'This is text A'
    assert b.file.value() == 'This is text B'


def test_excluded_files():
    echo = CommandRule.new(
        INPUT_VALUE, 'echo', commands=['echo $IN'],
        predicate=Predicates.simple_match(Predicates.IN_FILE_NAME, Predicates.string('b*')))
    test_dir = establish_directory('context', sources='txt_5')
    builder = Builder.new(test_dir, rules=[echo], sources=[test_dir / 'a.txt', test_dir / 'b.txt'])

    builder.get_value(builder.contexts[0], ValueName('echo'))
    pre = builder.get_to_evaluate().apply(capture_errors=False)
    builder.post_process(pre)
    assert pre.context.file.value() is None

    builder.get_value(builder.contexts[1], ValueName('echo'))
    pre = builder.get_to_evaluate().apply(capture_errors=False)
    builder.post_process(pre)
    assert pre.context.file.value() == 'b.txt'


def test_multiple_edges_to_node():
    a_rule = CommandRule.new(
        INPUT_VALUE, 'X', commands=['echo A'], predicate=Predicates.FALSE)
    b_rule = CommandRule.new(
        INPUT_VALUE, 'X', commands=['echo B'], predicate=Predicates.FALSE)
    c_rule = CommandRule.new(
        INPUT_VALUE, 'X', commands=['echo C'], predicate=Predicates.TRUE)
    test_dir = establish_directory('context', files={'a_file': ''})
    builder = Builder.new(test_dir, rules=[a_rule, b_rule, c_rule], sources=[test_dir / 'a_file'])

    builder.get_value(builder.contexts[0], ValueName('X'))
    pre = builder.get_to_evaluate().apply(capture_errors=False)
    assert pre.context.state == FileState.EVALUATE
    builder.try_next_rule(pre)
    pre = builder.get_to_evaluate().apply(capture_errors=False)
    assert pre.context.state == FileState.EVALUATE
    builder.try_next_rule(pre)
    pre = builder.get_to_evaluate().apply(capture_errors=False)
    assert pre.context.state == FileState.EVALUATE
    builder.post_process(pre)
    assert pre.context.state == FileState.READY
    assert pre.context.file.value() == 'C'


def test_alias_rules():
    a_rule = CommandRule.new(INPUT_VALUE, 'a')
    b_rule = CommandRule.new('a', 'b')
    c_rule = CommandRule.new('b', 'c')
    d_rule = CommandRule.new(INPUT_VALUE, 'd', commands=['echo $c'], predicate=Predicates.ref('c'))
    test_dir = establish_directory('context', files={'a_file': 'XxX'})
    builder = Builder.new(test_dir, rules=[a_rule, b_rule, c_rule, d_rule], sources=[test_dir / 'a_file'])

    builder.get_value(builder.contexts[0], ValueName('d'))
    pre = builder.get_to_evaluate().apply(capture_errors=False)
    builder.post_process(pre)
    assert pre.context.file.value() == 'XxX'
