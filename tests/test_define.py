from minion.file_facade import Facade
from minion.value import ValueName
from tests.test_context import establish_directory


def test_define_value():
    test_dir = establish_directory('context', rule_file='define', sources='txt_2')
    facade = Facade.new(test_dir, sources=[test_dir / 'a.txt', test_dir / 'b.txt'])
    facade.define('defined', 'ABC')
    f = facade.get_data_file(test_dir / 'a.txt.d' / 'echo')
    assert f.value() == 'a.txt: ABC'
    f = facade.get_data_file(test_dir / 'b.txt.d' / 'echo')
    assert f.value() == 'b.txt: ABC'


def test_say_rule():
    test_dir = establish_directory('context', rule_file='say')
    facade = Facade.new(test_dir)
    facade.define('message', 'Hello, world!')
    f = facade.get_data_file(test_dir / 'say')
    assert f.value() == 'Hello, world!'
