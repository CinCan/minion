import pathlib
import shutil

from minion.file_facade import Facade
from tests.test_context import establish_directory


def test_import_basic():
    test_dir = establish_directory('context', rule_file='import_unzip_redefine_strings', sources='zip_txt')
    shutil.copy(pathlib.Path() / 'tests' / 'rules' / 'type_strings_unzip.rules', test_dir.path)
    facade = Facade.new(test_dir, sources=[test_dir / 'txt.zip'])

    f = facade.get_data_file(test_dir / 'txt.zip.d' / 'IN' / 'a.txt.d' / 'strings')
    assert f.value() == 'strings txt.zip.d/IN/a.txt'


def test_import_connect_rule_sets():
    test_dir = establish_directory('context', rule_file='import_connect_rule_sets', sources='zip_txt')
    shutil.copy(pathlib.Path() / 'tests' / 'rules' / 'type_strings_unzip.rules', test_dir.path)
    shutil.copy(pathlib.Path() / 'tests' / 'rules' / 'copy.rules', test_dir.path)
    facade = Facade.new(test_dir, sources=[test_dir / 'txt.zip'])

    facade.build_all()
    f = sorted(facade.manager.root_file.list_files(recurse=True))
    assert f[0].as_string() == 'copy.rules'
    assert f[1].as_string() == 'minion.rules'
    assert f[2].as_string() == 'txt.zip'
    # assert f[ ].as_string() == 'txt.zip.d/file.copy'
    assert f[4].as_string() == 'txt.zip.d/IN/a.txt.d/file.copy'
    assert f[8].as_string() == 'txt.zip.d/IN/b.txt.d/file.copy'
    assert f[11].as_string() == 'txt.zip.d/strings'
    assert f[12].as_string() == 'txt.zip.d/type'
    assert len(f) == 14
