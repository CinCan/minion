from tests.test_cookbook_basic import copy_from_cookbook


def test_define_rules():
    out = copy_from_cookbook("sensitive", "define_value.sh")
    print(out)
    c = sorted(out.strip().split('\n'))
    assert c[:4] == ['Hello, unzipped world']


def test_environment_value():
    out = copy_from_cookbook("sensitive", "environment_value.sh")
    print(out)
    c = sorted(out.strip().split('\n'))
    assert c[:4] == ['Hello, unzipped world']
