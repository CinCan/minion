from datetime import datetime

from minion.interfaces import FileLog


def test_time():
    s = FileLog.format_time(datetime(2020, 2, 3, 4, 5, 6))
    assert s == '2020-02-03T04:05:06.000000'

    s = FileLog.format_time(datetime(1, 2, 3, 4, 5, 6))
    assert s == '0001-02-03T04:05:06.000000'

    s = FileLog.format_time(datetime(22, 2, 3, 4, 5, 6))
    assert s == '0022-02-03T04:05:06.000000'

    s = FileLog.format_time(datetime(333, 2, 3, 4, 5, 6))
    assert s == '0333-02-03T04:05:06.000000'
