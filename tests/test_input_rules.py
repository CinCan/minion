from minion.file_facade import Facade
from minion.value import ValueName
from tests.test_context import establish_directory


def test_file_input():
    test_dir = establish_directory('context', rule_file='file_input', sources='txt_3')
    facade = Facade.new(test_dir)
    f = facade.find_values(ValueName('content'))
    assert f[0].value() == 'This is text A'
    assert f[1].value() == 'This is text B'
    assert f[2].value() == 'This is text C'
    assert len(f) == 3


def test_file_input_alias():
    test_dir = establish_directory('context', rule_file='file_input_alias', sources='txt_3')
    facade = Facade.new(test_dir)
    f = facade.find_values(ValueName('print'))
    assert f[0].value() == 'a.txt'
    assert f[1].value() == 'b.txt'
    assert f[2].value() == 'c.txt'
    assert len(f) == 3


def test_file_input_alias():
    test_dir = establish_directory('context', rule_file='file_input_name_filter', sources='txt_3')
    facade = Facade.new(test_dir)
    f = facade.find_values(ValueName('print'))
    assert f[0].value() == 'This is text B: a.txt'
    assert f[1].value() == 'This is text B: c.txt'
    assert len(f) == 2
