from minion.file import DataFile
from minion.file_facade import Facade
from minion.value import ValueName
from tests.test_context import establish_directory
from tests.test_cookbook_basic import copy_from_cookbook


def test_different_dir_variants():
    test_dir = establish_directory('context', rule_file='different_directory_inputs', sources='zip_txt')
    facade = Facade.new(test_dir, sources=[test_dir / 'txt.zip'])

    f = facade.find_values(ValueName('echo-file'))
    assert f[0].as_string() == 'txt.zip.d/files/a.txt.d/echo-file'
    assert f[1].as_string() == 'txt.zip.d/files/b.txt.d/echo-file'
    assert len(f) == 2

    f = facade.find_values(ValueName('echo-dir'))
    assert f[0].as_string() == 'txt.zip.d/files.d/echo-dir'
    assert len(f) == 1

    f = facade.find_values(ValueName('echo-pre'))
    assert f[0].as_string() == 'txt.zip.d/echo-pre'
    assert len(f) == 1


def test_index_from_cookbook():
    test_dir = DataFile.root_file(copy_from_cookbook('textdata'))
    facade = Facade.new(test_dir, rule_file=test_dir / 'index-data.rules', sources=[test_dir / 'pslist.txt'])
    f = facade.find_values(ValueName('summary.csv'))
    with open('tests/test_index_data.out') as r:
        assert r.read().strip() == f[0].value()
    assert len(f) == 1
