import pathlib

import pytest

from minion.file import DataFile
from minion.file_lexer import parse
from minion.predicate import Predicates
from minion.rule_library import FileBasedRuleLibrary
from minion.rules import CommandRule
from minion.value import INPUT_VALUE, ValueName, GLOBAL


def test_rule_string():
    type_rule = CommandRule.new(
        INPUT_VALUE, 'type', commands=['file -b --mime-type $IN'])
    echo_rule = CommandRule.new(
        INPUT_VALUE, 'echo', commands=['echo $md'], predicate=Predicates.ref('md'))
    md_rule = CommandRule.new(
        'type', 'md', commands=['cat $type | md5sum'])
    unzip_rule = CommandRule.new(
        INPUT_VALUE, INPUT_VALUE, commands=['unzip $IN -d $OUT'],
        predicate=Predicates.equal(Predicates.ref('type'), Predicates.string("application/zip")))

    assert type_rule.to_string() == "rule IN => type\n\tfile -b --mime-type $IN"
    assert type_rule.digest == '7da9260e51e0f15a'
    assert echo_rule.to_string() == "rule IN with md => echo\n\techo $md"
    assert echo_rule.digest == '3d14b2be34325844'
    assert md_rule.to_string() == "rule type => md\n\tcat $type | md5sum"
    assert md_rule.digest == 'f79a11067999bdae'
    assert unzip_rule.to_string() == 'rule IN with type == "application/zip" => IN\n\tunzip $IN -d $OUT'
    assert unzip_rule.digest == 'b4d7f48dfe732cfa'


def test_parse_rule():
    a_rule = parse('rule a => b', pathlib.Path('x'))[0]
    assert a_rule.start == ValueName('a')
    assert a_rule.end == ValueName('b')
    assert a_rule.predicate is None
    assert a_rule.to_string() == 'rule a => b'

    a_rule = parse('rule a with c and not d => b', pathlib.Path('x'))[0]
    assert a_rule.start == ValueName('a')
    assert a_rule.end == ValueName('b')
    assert a_rule.predicate.__str__() == '(c and not d)'

    a_rule = parse('rule IN with t == "X" => b\n\t echo $IN', pathlib.Path('x'))[0]
    assert a_rule.start == INPUT_VALUE
    assert a_rule.end == ValueName('b')
    assert a_rule.predicate.__str__() == 't == "X"'

    a_rule = parse('rule IN with IN:t => the-end.tm', pathlib.Path('x'))[0]
    assert a_rule.start == INPUT_VALUE
    assert a_rule.end == ValueName('the-end.tm')
    assert a_rule.predicate.__str__() == 'IN:t'

    a_rule = parse('rule a => b --target --batch', pathlib.Path('x'))[0]
    assert a_rule.start == ValueName('a')
    assert a_rule.end == ValueName('b')
    assert a_rule.predicate is None
    assert a_rule.to_string() == 'rule a => b --batch --target'


def test_rule_parsing_errors():
    with pytest.raises(Exception) as e:
        r = parse('\n\nrule a >> b\n', pathlib.Path('x.rules'))[0]
    assert str(e.value) == "x.rules: No terminal defined for '>' at line 3 col 8\n\nrule a >> b\n       ^"

    with pytest.raises(Exception) as e:
        r = parse('\n\nrule a => b --option\n', pathlib.Path('x.rules'))[0]
    assert str(e.value) == "x.rules: Unexpected token Token(PATH, '--option') at line 3, column 13."


def test_rule_reachability():
    lib = FileBasedRuleLibrary(DataFile(pathlib.Path() / 'minion' / 'rules' / 'samples' / 'pdf-pipeline.rules'))

    rules = sorted(lib.get_rules_to_find(GLOBAL, ValueName('pdf.json')))
    assert not rules
    rules = sorted(lib.get_rules_to_find(GLOBAL, INPUT_VALUE))
    assert not rules
    rules = sorted(lib.get_rules_to_find(GLOBAL, ValueName('summary.json')))
    assert len(rules) == 1
    assert rules[0].digest == '6250e82e534a7275'

    rules = sorted(lib.get_rules_to_find(INPUT_VALUE, ValueName('pdf.json')))
    assert len(rules) == 4
    assert rules[0].digest == 'b4d7f48dfe732cfa'
    assert rules[1].digest == 'e1be1e1a6b2891ce'
    assert rules[2].digest == 'e179c5b0271ba3b2'
    assert rules[3].digest == '4ae2978fc4f984ff'

    rules = sorted(lib.list_reachable_values(ValueName('pdf')))
    assert rules == [ValueName('da'), ValueName('jsunpack'), ValueName('pdfid')]
    rules = sorted(lib.list_reachable_values(GLOBAL))
    assert rules == [ValueName('summary.json')]
    rules = sorted(lib.list_reachable_values(INPUT_VALUE))
    assert rules == [ValueName('IN'), ValueName('clamav'), ValueName('da'), ValueName('docx'), ValueName('docx.json'),
                     ValueName('jsunpack'), ValueName('oledump'), ValueName('pdf'), ValueName('pdf.json'),
                     ValueName('pdfid'), ValueName('sha256'), ValueName('strings'), ValueName('type')]

    assert not lib.is_reachable(ValueName('nosuch'))
    assert lib.is_reachable(ValueName('pdf.json'))
    assert lib.is_reachable(ValueName('type'))
    assert lib.is_reachable(ValueName('summary.json'))
