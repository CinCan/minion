import logging

from minion.file_facade import Facade
from minion.value import ValueName
from tests.test_context import establish_directory


def check(caplog, log_level: int, log_line: str) -> bool:
    for r in caplog.records:
        if r.message == log_line:
            if r.levelno != log_level:
                raise Exception(f"{r} not on level {log_level}")
            return True
    return False


def test_log_build(caplog):
    caplog.set_level(logging.INFO)
    test_dir = establish_directory('context', rule_file='type_strings_unzip', sources='zip_txt')
    facade = Facade.new(test_dir, sources=[test_dir / 'txt.zip'])
    facade.build_all()
    log = [r.message for r in caplog.records if r.message.startswith('[')]
    assert log == [
        '[x] txt.zip.d/strings',
        '[x] txt.zip.d/type',
        '[x] txt.zip.d/IN',
        '[x] txt.zip.d/IN/a.txt.d/strings',
        '[x] txt.zip.d/IN/b.txt.d/strings',
        '[x] txt.zip.d/IN/b.txt.d/type',
        '[x] txt.zip.d/IN/a.txt.d/type'
    ]


def test_log_find(caplog):
    caplog.set_level(logging.INFO)
    test_dir = establish_directory('context', rule_file='type_strings_unzip', sources='zip_txt')
    facade = Facade.new(test_dir, sources=[test_dir / 'txt.zip'])
    facade.find_values(ValueName('type'), ValueName('strings'))
    log = [r.message for r in caplog.records]
    assert log == [
        '[x] txt.zip.d/type',
        'file -b --mime-type txt.zip > txt.zip.d/type',
        '[x] txt.zip.d/strings',
        'strings txt.zip > txt.zip.d/strings',
        '[x] txt.zip.d/IN',
        'unzip txt.zip -d txt.zip.d/IN',
        'Archive:  txt.zip',
        ' extracting: txt.zip.d/IN/a.txt      ',
        ' extracting: txt.zip.d/IN/b.txt      ',
        '',
        '[x] txt.zip.d/IN/a.txt.d/type',
        'file -b --mime-type txt.zip.d/IN/a.txt > txt.zip.d/IN/a.txt.d/type',
        '[x] txt.zip.d/IN/b.txt.d/type',
        'file -b --mime-type txt.zip.d/IN/b.txt > txt.zip.d/IN/b.txt.d/type',
        '[x] txt.zip.d/IN/b.txt.d/strings',
        'strings txt.zip.d/IN/b.txt > txt.zip.d/IN/b.txt.d/strings',
        '[x] txt.zip.d/IN/a.txt.d/strings',
        'strings txt.zip.d/IN/a.txt > txt.zip.d/IN/a.txt.d/strings'
    ]
    caplog.clear()

    # re-run
    facade = Facade.new(test_dir, sources=[test_dir / 'txt.zip'])
    facade.find_values(ValueName('type'), ValueName('strings'))
    log = [r.message for r in caplog.records]
    caplog.clear()
    assert log == [
        '[/] txt.zip.d/type',
        '[/] txt.zip.d/strings',
        '[/] txt.zip.d/IN',
        '[/] txt.zip.d/IN/a.txt.d/type',
        '[/] txt.zip.d/IN/b.txt.d/type',
        '[/] txt.zip.d/IN/b.txt.d/strings',
        '[/] txt.zip.d/IN/a.txt.d/strings'
    ]

    # modify rule digest to simulate rule edit
    facade = Facade.new(test_dir, sources=[test_dir / 'txt.zip'])
    d = 'cd80020846a09820'
    facade.manager.rules.get_rule(d).digest = '1'
    del facade.manager.rules.rules_by_digest[d]
    facade.manager.rules.rules_by_digest['1'] = facade.manager.rules.get_rule('1')
    facade.find_values(ValueName('type'), ValueName('strings'))
    log = [r.message for r in caplog.records]
    assert log == [
        '[/] txt.zip.d/type',
        '[x] txt.zip.d/strings',
        'strings txt.zip > txt.zip.d/strings',
        '[/] txt.zip.d/IN',
        '[/] txt.zip.d/IN/a.txt.d/type',
        '[/] txt.zip.d/IN/b.txt.d/type',
        '[x] txt.zip.d/IN/b.txt.d/strings',
        'strings txt.zip.d/IN/b.txt > txt.zip.d/IN/b.txt.d/strings',
        '[x] txt.zip.d/IN/a.txt.d/strings',
        'strings txt.zip.d/IN/a.txt > txt.zip.d/IN/a.txt.d/strings'
    ]


def test_bad_rule(caplog):
    caplog.set_level(logging.INFO)
    test_dir = establish_directory('context', rule_file='bad', sources='txt_1')
    facade = Facade.new(test_dir, sources=[test_dir / 'a.txt'])

    facade.build_all()
    assert check(caplog, logging.INFO, '[E] a.txt.d/bad')
    assert check(
        caplog, logging.ERROR,
        'Exit code 127 for rule IN => bad # bb710dd8cb2fcd1d (/home/rauli/cincan/minion/tests/_context/minion.rules line 1)')


def test_debug_target(caplog):
    caplog.set_level(logging.INFO)
    test_dir = establish_directory('context', rule_file='type_strings_unzip', sources='zip_txt')
    facade = Facade.new(test_dir, sources=[test_dir / 'txt.zip'])
    facade.builder.debug_targets = {ValueName('IN')}

    facade.build_all()

    assert check(caplog, logging.INFO, '[x] txt.zip.d/type')
    assert check(caplog, logging.DEBUG, '--- txt.zip.d/IN')
    assert check(caplog, logging.DEBUG, 'IN = txt.zip')
    assert check(caplog, logging.DEBUG, 'type = txt.zip.d/type')
    assert check(caplog, logging.INFO, '[x] txt.zip.d/IN')
