# Minion cookbook

Here are some recipes for using Minion. 
Some of them are very simple to understand how Minion work.
Some (hopefully many) solve problems which more resemble real-world challenges.

Recipes:

 * [Basic recipes](Basic.md) to understand how Minion works
 * [Process text data](TextData.md) to manipulate textual data in tables, logs, etc.
 * [Process file and directory](Files.md) structures
 * Make queries by HTTP etc. (TO BE DONE)
 * [Import rules](RuleImport.md) to split rule files or to reuse rules
 * [Handling sensitive inputs](SensitiveData.md) to Minion rules
 * [Optimize processing](Optimize.md) by *batch rules* and *piping of data*

