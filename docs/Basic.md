# Basics

The following illustrates the basic operation of Minion

For the following we assume your working directory is `<minion-install>/docs/basic`

## Recipe: Cat file

The first Minion rule set uses built-in input `IN` and outputs file `cat-file`.
The output is created by the command `cat`:

```
rule IN => cat-file
    cat $IN
```

Run it like this:

```
$ minion -r cat-file.rules build hello.txt
```

Where `build` is the Minion sub-command, `cat-file.rules` is the rule file from above,
and `hello.txt` is name of input file we provide to Minion.

What you get should be like this:

```
08:16:52 [x] hello.txt.d/cat-file
08:16:52 cat hello.txt > hello.txt.d/cat-file
```

The sub-command `build` builds all possible rules.
We can see the result by looking at the output file.
The log shows that output from the `cat` command was directed to file `hello.txt.d/cat-file`:

```
$ cat hello.txt.d/cat-file
Hello, World!
```

### Command `get`

Using command `get` instead of `build` allows you to get access to the result:

```
$ minion -q -r cat-file.rules get cat-file
hello.txt.d/cat-file
```

The command `get` is followed by the name of the output file (we call it *target*).
Option `-q` is for quieter output.

And if you wish to get the actual content and not the file for it, use command line:

```
$ minion -qc -r cat-file.rules get cat-file
Hello, World!
```

Here `-c` stands for content output.

## Recipe: Rule predicate

A rule predicate allows to process files differently depending on their characteristics.
The following processes files `*.txt` and `*.bin` differently.

```
rule IN // "*.txt" => print
    cat $IN

rule IN // "*.bin" => digest
    md5sum $IN
```

Invoke it for two sample files `hello.txt` and `encoded.bin` like this:

```
$ minion -r rule-predicate.rules build hello.txt encoded.bin
08:55:27 [x] encoded.bin.d/digest
08:55:27 [x] hello.txt.d/print
08:55:27 md5sum encoded.bin > encoded.bin.d/digest
08:55:27 cat hello.txt > hello.txt.d/print
```

You can see the result files from the log, but they are two different files.
The next recipe shows you how to combine output from several rules.

## Recipe: Summary

The following rule set processes `*.txt` and `*.bin` files in similar fashion
that the earlier, but it also has a *summary* target which creates a summary of all files.

```
rule IN // "*.txt" => print
    echo `cat $IN`  $IN

rule IN // "*.bin" => digest
    md5sum $IN

rule summary <= print, digest --target
    cat $print $digest
```

A rule with left-arrow input `<=` creates a single output file even when there are many 
input files.
Predicate `print, digest` says that the rule is applied when there is one or more
`print` outputs or `digest` outputs to process.
The rule option `--target` means that the rule is a *default target* for the rule file.
With default target defined, we can use `use` command, which resolves the default target(s):

```
$ minion -c -r summary.rules use hello.txt encoded.bin
11:23:24 [x] hello.txt.d/print
11:23:24 [x] encoded.bin.d/digest
11:23:24 echo `cat hello.txt`  hello.txt > hello.txt.d/print
11:23:24 md5sum encoded.bin > encoded.bin.d/digest
11:23:24 [x] summary
11:23:24 cat hello.txt.d/print encoded.bin.d/digest > summary
Hello, World! hello.txt
c3220f67afa12591543b25902cc2fc98  encoded.bin
```

The last two lines are the actual summary output (with `-q` you can get only them).

## Recipe: File type

Command line tool `file` provides a nice way to probe file types which
can be then used in rule predicate to choose proper action.
The following rule file uses `file` to detect zip-files and then unzips then.
Text files are printed.

```
rule IN => type
    file -b --mime-type $IN

rule IN with type == "application/zip" => IN
    unzip $IN -d $OUT

rule IN with type == "text/plain" => print --target
    cat $IN
```

The unzipping rule uses pattern `IN => IN` to re-apply the same rules to the unzipped
output files than are applied to input files.
Now this rule set works as well for zipped text files as for text files provided as it is:

```
$ minion -cq -r type-and-unzip.rules use abc.zip
Sample file A
Sample file B
Sample file C
```

```
$ minion -cq -r type-and-unzip.rules use hello.txt
Hello, World!
```

## Recipe: Alternative rules

Sometimes you must apply alternative processing depending on predicate.
Consider the following:

```
rule IN // "*.bin" => plain
    base64 -d $IN

rule IN // "*.txt" => plain

rule plain => print --target
    cat $plain
```

Above processes files from `IN` into files `plain` and then to output files `print`.
Base64 encoded `*.bin` files are decoded into plain files.
Text files do not need any action, so the rule does not have any commands (it is just an *alias*).
