# Handling sensitive input

When you have passwords, API keys, etc. which you should not share with the community,
you should not save them into rule files or data files.

## Define values in command line

Minion supports giving them by command-line (remember that on that case they may be
visible in process lists).

Minion command line argument `--define` or `-D` defines a value, which can be used
in a rule like any value, e.g.

```
rule IN // "*.zip" with password => unpack
    unzip -P "$password" $IN -d $OUT
```

Now you can invoke minion and define "password":

```
minion -D"password=Secret" use minion.rules sample.zip
```

## Use environment variables

Alternatively you can define them using environment variables, which do get inherited
into minion rule commands 
(note that the environment gets also copied into invoked commands).

```
rule IN // "*.zip" => unpack
    unzip -P "$PASSWORD" $IN -d $OUT
```

Now you can invoke minion with defining PASSWORD environment variable:

```
PASSWORD="Secret" minion use minion.rules sample.zip
```