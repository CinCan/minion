# Optimizing Minion processing

# Parallel operation

Minion tries to run the rules in parallel as default.
The maximum number of parallel rules is by default to the number of cores in the machine.
It is possible to change the default thread maximum by option `--threads`:

```
$ minion --threads 20 use <inputs> ...
```

# Recipe: Stdin pipes

For very large number of files the maximum shell command-line length may be reached
(perhaps around 130k characters).

To work around this problem, one can provide input for rule command through stdin.
Below, the last rule command prefix `$(file-output) |` means that the input files
are provided for the trailing command through stdin.

```
rule IN --dir => IN

rule IN => file-output
    cat $IN

rule all-output <= file-output --target
    $(file-output) | cat
```



```
$ minion -c use stdin-pipe.rules *.txt
cat a-file.txt > a-file.txt.d/file-output
cat c-file.txt > c-file.txt.d/file-output
cat b-file.txt > b-file.txt.d/file-output
cat d-file.txt > d-file.txt.d/file-output
cat a-file.txt.d/file-output b-file.txt.d/file-output c-file.txt.d/file-output d-file.txt.d/file-output | cat > all-output
File A
File B
File C
File D
```

Even that the log would imply that all input files are cat'ed in, they are actually 
provided through stdin directly. The command logged command line is cut short when
the number of input files exceed 20.

# Recipe: Batch rules

Some command are slow to invoke, but can then process a lot of files fast at once.
For this, a rule can be marked with option `--batch` to invoke all rule processing
at once, even when rule produces many different outputs from many different inputs.

Consider the following rule set, which uses `sed` interactive mode to process
a set of files in place at once. The files are first copied into output directory,
and then processed.

```
rule IN => out-file --batch
    mkdir -p $OUT
    cp $IN $OUT
    sed -i "s/File/Elif/" $OUT/*
```

The output log of the rule also illustrates how a batch rule operates:

```
$ minion use batch.rules *.txt
12:21:34 [_] a-file.txt.d/out-file
12:21:34 [_] c-file.txt.d/out-file
12:21:34 [_] d-file.txt.d/out-file
12:21:34 [_] b-file.txt.d/out-file
12:21:34 [x] .minion/batch/out-file
mkdir -p .minion/batch/out-file
cp a-file.txt b-file.txt c-file.txt d-file.txt .minion/batch/out-file
sed -i "s/File/Elif/" .minion/batch/out-file/*
12:21:34 mv .minion/batch/out-file/a-file.txt -> a-file.txt.d/out-file
12:21:34 mv .minion/batch/out-file/b-file.txt -> b-file.txt.d/out-file
12:21:34 mv .minion/batch/out-file/c-file.txt -> c-file.txt.d/out-file
12:21:34 mv .minion/batch/out-file/d-file.txt -> d-file.txt.d/out-file
a-file.txt.d/out-file
b-file.txt.d/out-file
c-file.txt.d/out-file
d-file.txt.d/out-file
```

The log also shows how Minion moves the result files into proper places.
