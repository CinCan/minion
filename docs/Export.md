# Exporting rule sets

It is possible to export rules into UML Activity Diagram -live images.
This is done with help of **Plant UML** server (https://plantuml.com/), which can be setup 
locally e.g. using Docker. It is also possible to call the public cloud
server instance (which naturally transports your rules into the cloud).

E.g. the "pdf-pipeline" rule set renders into the following image:

![pdf-pipeline activity diagram](pdf-pipeline-ad.png)

## Export options

The general command line to export rule file is the following

   $ minion export FORMAT RULE-FILE

Where FORMAT is `plantuml-ad` to export as PlantUML activity diagram.
Format can be followed with some options.
You can use any valid Minion rule file for RULE-FILE.
As default the output is "puml" text file which PlantUML accepts as input.
You can process directly to PNG image format by providing URL to
processing server.

E.g. to use a local server running at port 8080 to export `samples/pdf-pipeline.rules`

    minion export --server http://localhost:8080/png/ -o rules.png plantuml-ad samples/pdf-pipeline.rules
    
We use option `-o` to save the output into a file.
 
You can also use the public cloud option (your model is shared with PlanUML server):

    minion export --server http://www.plantuml.com/plantuml/img/ -o rules.png plantuml-ad samples/pdf-pipeline.rules

## Local Plant UML server

The easiest way to set up a local Plant UML server is probably by a Docker image.

A successfully used image is "plantuml/plantuml-server" from Docker Hub.
