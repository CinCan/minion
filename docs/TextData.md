# Process text data

The following recipes process data organized as text files with lines sharing identical structure.
This includes CSV (comma separated value) files, log files, etc.

For the following we assume your working directory is `<minion-install>/docs/textdata`

The example data we use is a memory dump process list
from command line tool `volatility`
of the [Volatility Foundation](https://www.volatilityfoundation.org/).
It is process list with data columns separated by vertical bars (pipes):

```
Offset(V)|Name|PID|PPID|Thds|Hnds|Sess|Wow64|Start|Exit
>|0x83d3ac58|System|4|0|81|516|-1|0|2019-10-28 15:47:28 UTC+0000|
>|0x844271a0|smss.exe|252|4|3|29|-1|0|2019-10-28 15:47:28 UTC+0000|
>|0x849a77b0|csrss.exe|328|320|8|527|0|0|2019-10-28 15:47:30 UTC+0000|
...13 lines omitted...
```

## Recipe: Print a line

The following very simple rule picks all lines with "svchost.exe" in them using `grep`.

```
rule IN => svchost
    grep "svchost.exe" $IN
```

The rule file works like this, listing five lines from our sample material:
```
$ minion -qc -r find-line.rules use pslist.txt
>|0x84a995e0|svchost.exe|584|460|15|371|0|0|2019-10-28 15:47:32 UTC+0000|
>|0x84ac49d0|svchost.exe|700|460|13|287|0|0|2019-10-28 15:47:32 UTC+0000|
...
```

## Recipe: Print a column

The following rule picks column 4 using `cut` command.

```
rule IN => pid-column
    grep ">|" $IN | cut -d"|" -f4
```

Rule set works like this:
```
$ minion -qc -r print-column.rules use pslist.txt
4
252
...
```

## Recipe: Group data by key

After the simple recipes, it is time to do something less obvious.
Ofter you need to group the data by some key. For example, group log by IP address to analyze
the traffic from one host.

Here we address the problem of analyzing data per process name.
We use a simple command-line tool `linemux` developed with Minion, but which is independent from it.

The following rule (I know, many '|'s) is doing the following:
 1. Use `grep` to filter away header line
 2. Use `awk` to insert process name as first item to new column
 3. Use `linemux` to group lines by the new first column

```
rule IN => process-by-name
    grep ">|" $IN | awk -F"|" '{print $3 "|" $0}' | linemux -k ".+?\|" -K -d $OUT
```

This works like this:

```
$ minion -q -r group-by-key.rules use pslist.txt
pslist.txt.d/process-by-name/System
pslist.txt.d/process-by-name/VBoxService.ex
pslist.txt.d/process-by-name/csrss.exe
pslist.txt.d/process-by-name/lsass.exe
pslist.txt.d/process-by-name/lsm.exe
pslist.txt.d/process-by-name/services.exe
pslist.txt.d/process-by-name/smss.exe
pslist.txt.d/process-by-name/svchost.exe
pslist.txt.d/process-by-name/wininit.exe
pslist.txt.d/process-by-name/winlogon.exe
```

The output lists the files which are created for each process name.
The content of the files are the matching files from the process list, e.g.
for key "svchost.exe":

```
$ cat pslist.txt.d/process-by-name/svchost.exe
svchost.exe|>|0x84a995e0|svchost.exe|584|460|15|371|0|0|2019-10-28 15:47:32 UTC+0000|
svchost.exe|>|0x84ac49d0|svchost.exe|700|460|13|287|0|0|2019-10-28 15:47:32 UTC+0000|
svchost.exe|>|0x84adf6c0|svchost.exe|760|460|21|400|0|0|2019-10-28 15:47:32 UTC+0000|
svchost.exe|>|0x84b0d540|svchost.exe|868|460|23|399|0|0|2019-10-28 15:47:33 UTC+0000|
svchost.exe|>|0x84b1c030|svchost.exe|908|460|24|376|0|0|2019-10-28 15:47:33 UTC+0000|
```

## Recipe: Enrich group data

Once we have established the data groups in previous recipe,
we can aggregate data per group.

The following rule set creates output "process" for each different process name
and then for each of them resolves a few values "name", "count", and "pids".
These values are then collected into a CSV file "process.csv" for each process,
which are finally collected into "summary.csv".

```
rule IN => process
    grep ">|" $IN | awk -F"|" '{print $3 "|" $0}' | linemux -k ".+?\|" -K -d $OUT

rule process => name
    basename $process

rule process => count
    cat $process | wc -l

rule process => pids
    cut -d"|" -f4 $process | xargs

rule process with name and count and pids => process.csv
    echo "$value(name), $value(count), $value(pids)"

rule summary.csv <= process.csv
    cat $(process.csv)
```

This rule set works like this for the sample data set:

```
$ minion -qc -r enrich-group-data.rules get summary.csv pslist.txt
System, 1, 4
VBoxService.ex, 1, 648
csrss.exe, 2, 328 364
lsass.exe, 1, 468
lsm.exe, 1, 476
services.exe, 1, 460
smss.exe, 1, 252
svchost.exe, 5, 584 700 760 868 908
wininit.exe, 1, 372
winlogon.exe, 1, 400
```

## Recipe: Index data

Lets assume we have the following information file about what some processes mean.

```
$ cat cookbook/textdata/process-info.txt
svchost.exe: Service Host process
System: The system process
wininit.exe: Windows initializer
winlogon.exe: Windows sign-in process
```

Now, we want rules to pick the process info and merge it with other information we have
for the process. For this, we develop the rule set into the following,
note line starting with keyword `file`:

```
rule IN => process
    grep ">|" $IN | awk -F"|" '{print $3 "|" $0}' | linemux -k ".+?\|" -K -d $OUT

file "process-info.txt" => process-info-map
    linemux -k ".+?:" -K -d $OUT $file

rule process => name
    basename $process

rule process => count
    cat $process | wc -l

rule process => pids
    cut -d"|" -f4 $process | xargs

rule process with GLOBAL:process-info-map/[name] => process-info
    cat $(process-info-map)

rule process with name and count and pids and [process-info] => process.csv
    echo "$value(name), $value(count), $value(pids), $value(process-info)"

rule summary.csv <= process.csv
    cat $(process.csv)
```

The line which starts with `file` processes the file which name is given in 
double quotes. The file name available for commands in variable named `$file`.

The bit mystic predicate `GLOBAL:process-info-map/[name]` fetches "process-info-map"
directory file "name" (which is the process name) into output file "process-info".

NOTE: We need prefix `GLOBAL:` to access the `process-info-map` created
from the file "process-info.txt". Ugly, hah!
(Hopefully this gets fixed somehow in the future)

The Minion command-line for this rule goes like this:

```
$ minion -r index-data.rules get summary.csv pslist.txt
System, 1, 4, System: The system process
VBoxService.ex, 1, 648, 
csrss.exe, 2, 328 364, 
lsass.exe, 1, 468, 
lsm.exe, 1, 476, 
services.exe, 1, 460, 
smss.exe, 1, 252, 
svchost.exe, 5, 584 700 760 868 908, Service Host process
wininit.exe, 1, 372, Windows initializer
winlogon.exe, 1, 400, Windows sign-in process
```
