import json
import pathlib
import shutil
from io import TextIOBase
from logging import Logger
from typing import Optional, Dict, List, Set

from minion.predicate import PredicateValues
from minion.interfaces import DataFile, Rule, FileMetadata, FileRegistry, RuleLibrary, FileLog


class MetafilePointer:
    def __init__(self, path: pathlib.Path):
        self.path = path
        self.source: Optional['MetafilePointer'] = None
        self.derived: List['MetafilePointer'] = []
        self.include = False  # ...for something

    def include_derived(self, value: bool = True):
        for d in self.derived:
            d.include_derived(value)
        self.include = value

    def __repr__(self):
        if self.include:
            return '[x] ' + self.path.as_posix()
        return '[ ] ' + self.path.as_posix()


class DefaultFileRegistry(FileRegistry):
    def __init__(self, root_dir: pathlib.Path, project_name="minion.rules", rules: RuleLibrary = RuleLibrary()):
        super().__init__(rules, DataFile.new(root_dir, root_dir))
        self.root_dir = root_dir
        self.root_dir = self.root_dir.resolve()
        self.project_name = project_name
        self.project_dir = self.root_dir / ".minion" / project_name
        self.meta_root_dir = self.project_dir / "meta"
        self.batch_root_dir = root_dir / ".minion" / project_name / "batch"  # keep relative!
        self.metadata_cache: Dict[pathlib.Path, FileMetadata] = {}

    def check_for_project_directory(self):
        if not self.project_dir.exists():
            raise Exception(f"Could not locate .minion directory for '{self.project_name}' ({self.root_dir})")

    def get_file_by_path(self, path: str) -> Optional[DataFile]:
        abs_p = pathlib.Path(path).resolve()
        try:
            abs_p.relative_to(self.root_dir)  # throws if not inside root
        except ValueError:
            return None  # outside the root
        f = DataFile.new(self.root_dir, abs_p)
        return f

    def clean(self, file: DataFile, dry=False, force=False):
        self.logger.debug("clean %s", self.root_dir.as_posix())
        self.check_for_project_directory()
        self.__clean(self.root_file, dry=dry)

        batch_path = self.get_file_by_path(self.batch_root_dir.as_posix()).path.resolve()
        self.logger.debug(f"rm -rf {batch_path.as_posix()}")
        if not dry:
            shutil.rmtree(batch_path, ignore_errors=True)

        self.logger.debug(f"rm -rf {self.meta_root_dir.as_posix()}")
        if not dry:
            shutil.rmtree(self.meta_root_dir, ignore_errors=True)

        if force:
            self.logger.debug(f"rm -rf {self.project_dir}")
            if not dry:
                shutil.rmtree(self.project_dir, ignore_errors=True)

    def __clean(self, file: DataFile, dry: bool) -> bool:
        if file.name().startswith('.'):
            return False
        meta_p = self.get_meta_path(file)
        if meta_p.is_file():
            if file.is_file():
                self.logger.info(f"rm -f {file.as_string()}")
                if not dry:
                    file.path.unlink()
            elif file.is_dir():
                self.logger.info(f"rm -rf {file.as_string()}")
                if not dry:
                    shutil.rmtree(file.path, ignore_errors=True)
            return True
        elif file.is_dir():
            rm_all = True
            for p in file.list_files():
                rm_all = self.__clean(p, dry) and rm_all
            if rm_all:
                self.logger.info(f"rmdir {file.as_string()}")
                if not dry:
                    file.path.rmdir()
            return rm_all
        return False

    def apply_rule(self, predicate: PredicateValues, out_file: DataFile, rule: Rule,
                   logger: Logger) -> FileMetadata:
        # collect all input, and remove duplicates
        in_log = []
        for files in [(p.files or []) for p in predicate.iterate()]:
            in_log.extend([FileLog.for_file(f) for f in files])

        out_file.path.parent.mkdir(parents=True, exist_ok=True)

        m = FileMetadata(out_file, in_log, rule)
        self.__prepare_for_write(m)
        rule.apply(predicate, out_file, m, timeout=self.rules.rule_timeout, logger=logger)
        if m.exit_code == 0 and not out_file.exists():
            raise Exception(f"No output file {out_file.as_string()} created by rule {rule.__str__()}")
        return m

    def relocate_rule(self, in_file: DataFile, interim_file: DataFile, out_file: DataFile, rule: Rule) -> DataFile:
        if not out_file.path.parent.exists():
            # parent directory missing - create it and metafile for it
            # - we kind of assume only single level of directories created for values (bad)
            out_file.path.parent.mkdir(parents=True, exist_ok=True)
            file_logs = [FileLog.for_file(in_file)]
            parent_m = FileMetadata(out_file.parent(), file_logs)  # no rule
            parent_m_f = self.get_meta_path(out_file.parent())
            parent_m_f.parent.mkdir(exist_ok=True, parents=True)
            with parent_m_f.open("w") as f:
                json.dump(parent_m.to_json(), f)

        in_log = [FileLog.for_file(in_file)]
        m = FileMetadata(out_file, in_log, rule)
        self.__prepare_for_write(m)
        interim_file.path.rename(out_file.path)  # move!

        # write the metafile
        self.write_metafile(m)

        return out_file

    def __prepare_for_write(self, meta_file: FileMetadata):
        out_file = meta_file.file
        meta_path = self.get_meta_path(out_file)
        if meta_path.exists():
            # existing meta-file, we are confident to overwrite old value and meta file
            if out_file.is_file():
                self.logger.debug("rm %s", out_file.as_string())
                out_file.path.unlink()
            if out_file.is_dir():
                self.logger.debug("rm -rf %s", out_file.as_string())
                shutil.rmtree(out_file.path, ignore_errors=True)
            if meta_file.file.is_file():
                meta_file.file.path.unlink()  # ...remove meta file last
        if out_file.exists():
            # do not dare to remove files
            raise Exception(f"Cannot overwrite existing file {out_file.as_string()}")
        meta_path.parent.mkdir(exist_ok=True, parents=True)
        meta_path.touch()  # zero-length file marks write attempt

    def write_metafile(self, meta_file: FileMetadata):
        self.__write_collect_output_metafile(meta_file.file, meta_file.output)
        meta_f = self.get_meta_path(meta_file.file)
        meta_f.parent.mkdir(exist_ok=True, parents=True)
        with meta_f.open("w") as f:
            json.dump(meta_file.to_json(), f)

    def __write_collect_output_metafile(self, file: DataFile, files: List[FileLog]):
        if file.is_dir():
            for f in file.list_files(recurse=False):
                files.append(FileLog.for_file(f))
        elif file.is_file():
            files.append(FileLog.for_file(file))

    def get_meta_path(self, file: DataFile) -> pathlib.Path:
        p = self.meta_root_dir / file.relative_path
        meta_f = p.parent / (p.name + ".MF")
        return meta_f

    def get_original_path(self, meta_path: pathlib.Path) -> pathlib.Path:
        return self.root_dir / meta_path.relative_to(self.meta_root_dir).with_suffix('')

    def get_metadata(self, file: DataFile, parent_meta: bool = False, full_data=True) -> Optional[FileMetadata]:
        meta_f = self.get_meta_path(file)
        if not meta_f.exists():
            if parent_meta and not file.is_root():
                return self.get_metadata(file.parent(), full_data=full_data)
            return None
        if meta_f.stat().st_size == 0:
            # zero-length is failed rule, no rule nor source
            return FileMetadata(file)

        if meta_f in self.metadata_cache:
            cached = self.metadata_cache[meta_f]
            if not full_data or cached.full_data:
                return cached

        with meta_f.open("r") as f:
            js = json.load(f)
        m = self._read_file_metadata(file, js, full_data)
        if parent_meta and not m.rule and not file.is_root():
            # look for the metadata with rule info
            p_metadata = self.get_metadata(file.parent(), parent_meta, full_data)
            if p_metadata and p_metadata.rule:
                return p_metadata

        self.metadata_cache[meta_f] = m
        return m

    def get_batch_dir(self) -> pathlib.Path:
        return self.batch_root_dir

    def explain(self, writer: TextIOBase, file: DataFile, with_details: bool = False,
                explained_already: Set[DataFile] = None):
        if explained_already is None:
            explained_already = set()
        if file in explained_already:
            return
        explained_already.add(file)
        meta_f = self.get_metadata(file, parent_meta=True)
        if not meta_f:
            self.check_for_project_directory()
            return
        printed = False
        for in_file in meta_f.input or []:
            self.explain(writer, self.root_file / in_file.path, with_details, explained_already)
        if with_details:
            writer.write('\n' if printed else '')
            writer.write(f"### {file.as_string()} ###")
            printed = True
        if meta_f.rule:
            writer.write(f"mkdir -p {meta_f.file.parent().as_string()}")
            printed = True
        for line in meta_f.log or []:
            if line[0] == 'in':
                writer.write('\n' if printed else '')
                writer.write(line[1])
                printed = True
            elif with_details:
                writer.write('\n' if printed else '')
                writer.write(f"# {line[1]}")
                printed = True
        writer.write('\n' if printed else '')
