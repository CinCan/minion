from typing import Dict, Set, Optional, List, Iterable

import plantuml

from minion.interfaces import RuleLibrary, Rule
from minion.value import ValueName, GLOBAL, INPUT_VALUE


class JSONExport:
    def __init__(self, rules: RuleLibrary):
        self.rules = rules

    def do_export(self) -> Dict:
        nodes = []
        links = []
        for name, _ in self.rules.get_value_nodes().items():
            nodes.append({'id': name.name})
        for rule in self.rules.list_rules():
            vars = list(rule.variables.keys())
            if len(vars) > 1:
                # join predicates in separate node
                node = rule.digest
                nodes.append({'id': node})
                for src in sorted(vars):
                    links.append({'source': src, "target": node})
                links.append({'source': node, 'target': rule.end.name, "command": rule.__repr__()})
            else:
                links.append({'source': rule.start.name, "target": rule.end.name, "command": rule.__repr__()})
        return {'nodes': nodes, 'links': links}


class PlantUMLExport:
    def __init__(self, rules: RuleLibrary):
        self.rules = rules

    def export_text(self) -> str:
        lines = [f'(*) --> "IN"']
        dupes = set()
        for rule in self.rules.list_rules():
            start = f'"{rule.start.name}"'
            end = f'"{rule.end.name}"'
            line = f'{start} --> {end}'
            if line in dupes:
                continue
            dupes.add(line)
            if rule.start != GLOBAL:
                lines.append(line)
            cmd_vars = rule.get_command_variables()
            for p in [r.name for r in rule.variables.values()]:
                if p == rule.start:
                    continue
                start = f'"{p.name}"'
                if p.name in cmd_vars:
                    lines.append(f'{start} --> {end}')
                else:
                    lines.append(f'{start} -[dotted]-> {end}')
        return "@startuml\n" + "\n".join(lines) + "\n@enduml"

    def export_image(self, url: Optional[str] = None) -> bytes:
        text = self.export_text()
        server = plantuml.PlantUML(url)
        raw = server.processes(text)
        return raw


class RuleLink:
    def __init__(self, value: ValueName):
        self.value = value
        self.rules: List[Rule] = []
        self.links: Dict[ValueName, 'RuleLink'] = {}

    def __str__(self):
        return self.value.name

    def end_to_start(self, library: RuleLibrary, rule: Rule, exclude: Set[Rule] = None) -> 'RuleLink':
        self.rules.append(rule)
        r_exc = exclude.copy() if exclude else set()
        for p in [rule.start] + [r.name for r in rule.variables.values()]:
            p_link = RuleLink(p)
            for r in [r for r in library.list_rules() if r.end == p]:
                if r not in r_exc:
                    r_exc.add(r)
                    p_link.end_to_start(library, r, r_exc)
            if p_link.rules:
                self.links[p] = p_link
        return self


class EBNFExport:
    def __init__(self, rules: RuleLibrary):
        self.rules = rules

    def do_export(self) -> str:
        lines = []
        dupes = set()

        intermediates = set()
        list_rules = self.rules.list_rules()
        for rule in list_rules:
            intermediates.update(self.rules.list_reachable_rules(rule.end))
        list_rules = [r for r in list_rules if r not in intermediates]

        for rule in list_rules:
            rule_link = RuleLink(rule.end).end_to_start(self.rules, rule)
            left = rule.end.name
            right = self.__right_side(rule_link)
            line = f'{left} ::= {right}'
            if line not in dupes:
                lines.append(line)
                dupes.add(line)
        return "\n".join(lines) + "\n"

    def __filter(self, covered: Iterable[Rule]) -> List[Rule]:
        intermediates = set()
        for rule in covered:
            intermediates.update(self.rules.list_reachable_rules(rule.end))
        return [r for r in self.rules.list_rules() if r not in intermediates]

    def __right_side(self, link: RuleLink) -> str:
        right = ""
        for value, r_link in link.links.items():
            if value == link.value:
                continue
            r_str = self.__right_side(r_link)
            if r_str and right:
                right += " | "
            right += f"{r_str}"
        if right:
            right = f"({right}) "
        right += link.value.name
        return right

    def __list_preceding(self, rule: Rule) -> Set[ValueName]:
        s = set()
        s.update([r.end for r in self.rules.list_rules() if r.end == rule.start])
        s.update([r.name for r in rule.variables.values()])
        return s
