import pathlib
import re
from io import TextIOBase
from logging import Logger
from typing import Optional, Iterable, List, Dict

from minion.predicate import PredicateValues
from minion.file_registry import DefaultFileRegistry
from minion.interfaces import FileManager, FileRegistry, DataFile, Rule, RuleLibrary, FileMetadata, RuleOption

# Files postfixed with this are for management purposes
from minion.rule_library import FileBasedRuleLibrary

MANAGEMENT_POSTFIX = '.d'


class SubDirectoryFileManager(FileManager):
    def __init__(self, registry: FileRegistry):
        super().__init__(registry)
        self.ws_pattern = re.compile('\\s+')

    @classmethod
    def new(cls, root: DataFile, rules: List[Rule] = None, rule_file: Optional[DataFile] = None) -> FileManager:
        if rules is None:
            rule_lib = FileBasedRuleLibrary(rule_file or root / 'minion.rules', file_root=root)
        else:
            rule_lib = RuleLibrary(rules)
        return SubDirectoryFileManager(DefaultFileRegistry(root.path, rules=rule_lib))

    def is_data_file(self, file: DataFile) -> bool:
        if file == self.registry.root_file:
            # if root is not data, then we cannot find any data files, ever
            return True
        name = file.name()
        if self.ws_pattern.search(name):
            return False  # while space
        return not name.startswith('.') and not name.endswith(MANAGEMENT_POSTFIX)

    def get_sub_data_files(self, file: DataFile, recurse=False) -> List[DataFile]:
        if not file.is_root() and (not file.is_dir() or not self.is_data_file(file)):
            return []
        r = []
        for f in file.list_files():
            if f.is_file() or not recurse:
                r.append(f)
            elif recurse:
                r.extend(self.get_sub_data_files(f, recurse))
        return r

    def get_data_file(self, path: pathlib.Path) -> DataFile:
        if path.suffix == MANAGEMENT_POSTFIX:
            return self.registry.get_file_by_path(path.parent / path.name[:-2])
        else:
            return self.registry.get_file_by_path(path)

    def is_applied_source_file(self, file: DataFile) -> bool:
        t = self.__target_for(file)
        return t and t.exists()

    def get_target(self, file: DataFile, rule_name: str = None) -> Optional[DataFile]:
        t_path = self.__target_for(file, rule_name)
        if t_path is None:
            return None
        target = DataFile.new(self.registry.root_dir, t_path)
        return target

    def get_existing_targets(self, file: DataFile) -> Iterable[DataFile]:
        t_path = self.__target_for(file)
        if not t_path or not t_path.is_dir():
            return []
        return [DataFile.new(self.registry.root_dir, f) for f in t_path.iterdir()]

    def __target_for(self, source: DataFile, rule: str = None) -> Optional[pathlib.Path]:
        if not self.is_data_file(source):
            return None
        if source.is_root() and not rule:
            return None
        a_dir = source.path.parent / (source.name() + MANAGEMENT_POSTFIX)
        t = a_dir / rule if rule else a_dir
        return t

    def apply_rule(self, predicate: PredicateValues, out_file: DataFile, rule: Rule,
                   logger: Logger) -> FileMetadata:
        m = self.registry.apply_rule(predicate, out_file, rule, logger)
        if out_file.is_dir() and not rule.pack_files():
            # some output file names are not accepted
            self.__postprocess_target(out_file)
        # write metafile _after_ post processing
        self.registry.write_metafile(m)
        return m

    def __postprocess_target(self, file: DataFile) -> DataFile:
        name = file.name()
        no_ws = self.ws_pattern.sub('_', name)
        if name.startswith('.') or name.endswith(MANAGEMENT_POSTFIX) or name != no_ws:
            # this is not a good file name
            f_name = f"_{no_ws[1:]}" if no_ws.startswith('.') else no_ws
            path = file.path.with_name(f_name)
            name_base = path.as_posix()
            name_i = 0
            if f_name.endswith(MANAGEMENT_POSTFIX):
                new_name = f"{name_base}_{name_i}"
            else:
                new_name = name_base
            while pathlib.Path(new_name).exists():
                name_i += 1
                new_name = f"{name_base}_{name_i}"
            new_path = pathlib.Path(new_name)
            self.logger.info("rename {} -> {}".format(file.as_string(), new_path.name))
            file.path.rename(new_path)
            file = file.with_path(new_path)
        if file.is_dir():
            for f in file.list_files(file):
                self.__postprocess_target(f)
        return file

    def print_hierarchy(self, writer: TextIOBase, file: DataFile, indent: str = ''):
        meta_f = self.registry.get_metadata(file, parent_meta=True)
        writer.write(file.name())
        if meta_f and meta_f.rule:
            rule = meta_f.rule
            writer.write(f" {rule.start} => {rule.end}")
        writer.write('\n')

        all_sub_files = list(file.list_files())
        target_f = self.get_target(file)
        if target_f:
            all_sub_files.extend(target_f.list_files())
        sub_files = sorted(set([self.get_data_file(f.path) for f in all_sub_files]))
        for i, f in enumerate(sub_files):
            any_rules_here = self.__any_rules_here(f)
            if not any_rules_here:
                continue
            writer.write(indent)
            if i < len(sub_files) - 1:
                writer.write('|-')
                ind = indent + '| '
            else:
                writer.write(r'\-')
                ind = indent + '  '
            self.print_hierarchy(writer, f, ind)

    def __any_rules_here(self, file: DataFile) -> bool:
        meta_f = self.registry.get_metadata(file)
        if meta_f:
            return True
        for f in file.list_files():
            if self.__any_rules_here(f):
                return True
        target_f = self.get_target(file)
        if target_f:
            return self.__any_rules_here(target_f)
        return False
