import math
import shutil
import sys
import threading
import time
from typing import Optional

from minion.context import FileState, FILE_STATE_ORDER, FileContext
from minion.file_facade import Facade, FacadeListener


class LiveLogger(FacadeListener):
    def __init__(self, facade: Facade):
        self.facade = facade
        self.thread = threading.Thread(name='live_log', target=self.__loop, daemon=True)
        self.lock = threading.Condition()
        self.run = False
        self.start_time = time.time()
        self.interval = .25  # seconds
        self.thread.start()

    def start_facade(self):
        with self.lock:
            self.run = True
            self.lock.notify()

    def stop_facade(self):
        with self.lock:
            self.run = False
            self.lock.notify()
            self.__print()

    def __loop(self):
        with self.lock:
            while True:
                if self.run:
                    self.__print()
                    self.lock.wait(self.interval)
                else:
                    self.lock.wait()

    def __print(self):
        width, height = shutil.get_terminal_size((80, 20))  # fallback values
        sys.stdout.write("\033[2J" + "\033[H")

        # space for status line
        height -= 1

        elapsed_time = time.time() - self.start_time
        elapsed_secs = math.floor(elapsed_time) % 60
        elapsed_min = math.floor(elapsed_time / 60) % 60
        elapsed_hour = math.floor(elapsed_time / 3600)
        elapsed_10s = math.floor(elapsed_time * 10) % 10

        # running contexts
        running = sorted([p for p in self.facade.scheduler.list_running()
                          if p.context.start_time > 0 and p.context.state != FileState.DROPPED],
                         key=lambda p: p.context.start_time)
        running = running[-height:]

        # ready contexts
        ready = []
        if len(running) < height:
            ready = sorted([c for c in self.facade.scheduler.list_contexts()
                            if c.state.is_finished() and not c.is_excluded() and c.start_time],
                           key=lambda c: c.start_time + c.elapsed_time)
            if len(running) + len(ready) > height:
                ready = ready[-height - len(running):]

        # print ready
        for ctx in ready:
            self.__print_context(width, ctx, ctx.elapsed_time)

        # print status
        print("----- {:02}:{:02}:{:02}.{} -----".format(elapsed_hour, elapsed_min, elapsed_secs, elapsed_10s))

        # print running
        for pre in running:
            elapsed_time = time.time() - pre.context.start_time
            self.__print_context(width, pre.context, elapsed_time)

        sys.stdout.flush()


    def __print_context(self, width: int, context: FileContext, show_time: float = -1):
        if context.error:
            time_s = "ERROR"
        elif show_time >= 0:
            time_s = "{:03}.{}".format(math.floor(show_time), math.floor(show_time * 10 % 10))
        else:
            time_s = "---.- "
        timestamp_wid = 6
        name = context.file.as_string()
        if timestamp_wid + len(name) > width:
            name = "..." + name[-(width - timestamp_wid - 3):]
        print("{} {}".format(time_s, name))
