from typing import Union, Optional


class ValueName:
    def __init__(self, name: str):
        self.name = name

    @classmethod
    def of(cls, value: Union[str, 'ValueName']):
        if type(value) == ValueName:
            return value
        return ValueName(value)

    def __repr__(self) -> str:
        return self.name

    def __eq__(self, other) -> bool:
        return isinstance(other, ValueName) and self.name == other.name

    def __hash__(self):
        return self.name.__hash__()

    def __lt__(self, other):
        return self.name.__lt__(other.name)


# source file or directory
INPUT_VALUE = ValueName('IN')

# input file name
INPUT_NAME = ValueName('name')

# global context directory
GLOBAL = ValueName('GLOBAL')

# files expanded for a directory
EXPAND_FILES = ValueName('*')

# local-scope, value created here
LOCAL = ValueName('LOCAL')
