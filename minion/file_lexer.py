import itertools
import pathlib
from typing import List, Optional, Dict, Tuple

from lark import Lark, Transformer, v_args

from minion import predicate, interfaces
from minion.file import DataFile
from minion.interfaces import Rule, RuleOption, RuleLibrary
from minion.predicate import Predicates, PredicateReference, PredicateFixedFile
from minion.rules import CommandRule
from minion.value import ValueName, GLOBAL, INPUT_VALUE

action_grammar = """
    start: (import_file | input_file | rule)*
    import_file: "import" PATH import_name_list
    !input_file: "file" ESCAPED_STRING rule_right_arrow options commands
    !rule: "rule" left_name (rule_with_fast_predicate | rule_no_fast_predicate) options commands
    left_name: "()" | RELATIVE_PATH
    right_name: NAME
    rule_with_fast_predicate: fast_predicate options rule_right_arrow
    rule_no_fast_predicate: options (rule_left_arrow | rule_right_arrow)
    rule_left_arrow: "<=" top_pre with_predicate
    rule_right_arrow: with_predicate "=>" right_name
    fast_predicate: fast_name_equal | fast_name_match
    fast_name_equal: "==" escaped_string
    fast_name_match:  "//" escaped_string
    with_predicate: ["with" top_pre]
    commands: (command)*
    options: (options_batch | options_target | options_dir | options_scope)*
    options_batch: "--batch"
    options_target: "--target"
    options_dir: "--dir"
    options_scope: "--scope" NAME
    import_name_list: [import_name ("," import_name)*]
    import_name: NAME ["as" NAME]
    command: COMMAND

    top_pre: operand     -> operand
          | top_pre "," operand -> or_predicate
          | top_pre "and" operand -> and_predicate
          | top_pre "or" operand  -> or_predicate

    operand: "(" top_pre ")"         -> grouped_predicate
          | "[" top_pre "]"          -> optional_predicate
          | "not" operand            -> not_predicate
          | op_value "==" op_value   -> eq_predicate
          | op_value "!=" op_value   -> not_eq_predicate
          | op_value "//" op_value   -> string_match
          | op_value [ "/" path ]    -> op_value

    op_value: escaped_string | ref_string
    escaped_string: ESCAPED_STRING
    ref_string: NAME [":" NAME]

    path: path_component (path_index path_component)*

    path_component: [PATH]
    path_index: "[" op_value "]"

    WS.1: /[ \\t\\r\\n]+/
    %ignore WS

    COMMENT.1: /[ \\t]*#.*/
    %ignore COMMENT

    NAME: /[a-zA-Z_][a-zA-Z0-9_.-]*/

    %import common.ESCAPED_STRING -> ESCAPED_STRING

    PATH: /[a-zA-Z0-9._\\/-]+/

    RELATIVE_PATH: /[a-zA-Z0-9._][a-zA-Z0-9._\\/-]*/

    COMMAND.2: /\\n[ \t]+[\\S][^\\n]*/
"""


@v_args(inline=True)  # Affects the signatures of the methods
class RuleParsingTree(Transformer):
    def __init__(self, rule_library: RuleLibrary, rule_source_file: pathlib.Path, rule_file_root: Optional[DataFile]):
        super().__init__()
        self.rule_library = rule_library
        self.rule_source_file = rule_source_file
        self.rule_file_root = rule_file_root

    def start(self, *rules) -> List[Rule]:
        return list(itertools.chain(*rules))

    def import_file(self, file, names: Dict[str, str]):
        import_names = {ValueName(n): ValueName(v) for n, v in names.items()}
        self.rule_library.import_rules(file.value, import_names)
        return []

    def input_file(self, file_keyword, file_name, rule_body, options, commands):
        _, pre, right_name = rule_body

        file_pattern = file_name.value[1:-1]
        path_root = self.rule_file_root or DataFile.new(pathlib.Path())
        rules = []
        for p in sorted(path_root.path.glob(file_pattern)):
            file = path_root.with_path(p.resolve())
            end = right_name
            pred = PredicateFixedFile(file)
            rule = CommandRule.new(GLOBAL, end, predicate=pred, commands=commands)
            rule.input_file = file
            rule.source = self.rule_source_file.as_posix(), file_keyword.line
            rule.options = options
            rule.local_variables.add(ValueName("file"))  # ugly
            self.rule_library.add_rule(rule)
            rules.append(rule)
        return rules

    def rule(self, rule_keyword, left_name_path, rule_body, options2, commands):
        options1, pre, right_name = rule_body
        opts = {}
        opts.update(options1)
        opts.update(options2)

        if isinstance(left_name_path, ValueName):
            left_name = left_name_path
        elif '/' in left_name_path and not left_name_path.endswith("/"):
            if opts.get(RuleOption.SCOPE) != GLOBAL:
                raise Exception(f"Target with path/ can only be used relative to the work directory")
            path_split = left_name_path.rindex('/')
            left_name = ValueName(left_name_path[path_split + 1:])
            opts[RuleOption.OUTPUT_DIRECTORY] = left_name_path[0:path_split]
        else:
            left_name = ValueName(left_name_path)

        if RuleOption.SCOPE in opts:
            if left_name == INPUT_VALUE:
                raise Exception(f"Cannot use {left_name} as '<=' left-hand side")
            start = right_name  # <=
            end = left_name
        else:
            start = left_name  # =>
            end = right_name
        rule = CommandRule.new(start, end, predicate=pre, commands=commands)
        rule.source = self.rule_source_file.as_posix(), rule_keyword.line
        rule.options = opts
        self.rule_library.add_rule(rule)
        return [rule]

    def rule_with_fast_predicate(self, fast_predicate, options1, rule_body):
        options2, slow_predicate, right_name = rule_body
        pre = fast_predicate
        if pre is None:
            pre = slow_predicate
        elif slow_predicate:
            pre = predicate.PredicateAnd(pre, slow_predicate)
        opts = {}
        opts.update(options1)
        opts.update(options2)
        return opts, pre, right_name

    def rule_no_fast_predicate(self, options, rule_body):
        options_a, pre, right_name = rule_body
        opts = {}
        opts.update(options_a)
        opts.update(options)
        return opts, pre, right_name

    def rule_left_arrow(self, right_ref, right_pre):
        options = {RuleOption.SCOPE: GLOBAL}
        if right_pre:
            # end <= start with predicate
            pre = right_pre
            if not isinstance(right_ref, PredicateReference):
                raise Exception(f"Invalid <= right-hand side: {right_ref}")
            right_name = right_ref.reference.name
        elif isinstance(right_ref, PredicateReference):
            # end <= start
            pre = None
            right_name = right_ref.reference.name
        else:
            # end <= abc and def
            pre = right_ref
            right_name = GLOBAL
        return options, pre, right_name

    def rule_right_arrow(self, pre, right_name):
        return {}, pre, right_name

    def left_name(self, *name) -> str:
        if not name:
            return GLOBAL
        return name[0].value

    def right_name(self, name) -> ValueName:
        return ValueName(name.value)

    def fast_predicate(self, *args) -> Optional[predicate.PredicateOperation]:
        return args[0] if args else None

    def fast_name_equal(self, name) -> predicate.PredicateOperation:
        file_name = predicate.PredicateFileName(only_input_name=True)
        return predicate.PredicateEquals(file_name, name)

    def fast_name_match(self, pattern) -> predicate.PredicateOperation:
        file_name = predicate.PredicateFileName(only_input_name=True)
        return predicate.PredicateSimpleMatch(file_name, pattern)

    def with_predicate(self, *args) -> Optional[predicate.PredicateOperation]:
        if args:
            return args[0]
        return None

    def predicate(self, *args):
        if not args:
            return None
        return args[0]

    def commands(self, *args):
        return args

    def options(self, *option):
        r = {}
        for opt in option:
            r.update(opt)
        return r

    def options_batch(self, *args):
        return {RuleOption.BATCH: True}

    def options_target(self, *args):
        return {RuleOption.TARGET: True}

    def options_dir(self, *args):
        return {RuleOption.DIRECTORY: True}

    def options_scope(self, ref):
        return {RuleOption.SCOPE: ValueName(ref.value)}

    def command(self, line):
        return line.value.strip()

    def and_predicate(self, left, right):
        return predicate.PredicateAnd(left, right)

    def or_predicate(self, left, right):
        return predicate.PredicateOr(left, right)

    def not_predicate(self, sub):
        return predicate.PredicateNot(sub)

    def operand(self, sub):
        return sub

    def grouped_predicate(self, sub):
        return sub

    def optional_predicate(self, sub):
        return predicate.PredicateOptional(sub)

    def op_value(self, *sub):
        if len(sub) > 1:
            return predicate.PredicatePattern(sub[0], sub[1])
        return sub[0]

    def escaped_string(self, value):
        s = predicate.unquote(value.value)
        return predicate.PredicateConstant(s)

    def ref_string(self, *name):
        if len(name) == 1:
            v = name[0].value
            if v == 'name':
                return Predicates.IN_FILE_NAME
            return predicate.PredicateReference(predicate.RuleReference.new(v))
        scope = name[0].value
        v = name[1].value
        return predicate.PredicateReference(predicate.RuleReference.new(v, scope))

    def eq_predicate(self, left, right):
        return predicate.PredicateEquals(left, right)

    def not_eq_predicate(self, left, right):
        return predicate.PredicateNot(predicate.PredicateEquals(left, right))

    def string_match(self, sub, pattern):
        return predicate.PredicateSimpleMatch(sub, pattern)

    def path(self, *sub):
        values = [s for s in sub if s]
        return predicate.PredicateSequence.sequence(*values)

    def path_component(self, *value):
        if value:
            return predicate.PredicateConstant(value[0].value)
        return None

    def path_index(self, sub):
        return predicate.PredicateIndex(sub)

    def import_name_list(self, *args) -> Dict[str, str]:
        return {a[0]: a[1] for a in args}

    def import_name(self, *args) -> Tuple[str, str]:
        if len(args) == 1:
            return args[0].value, args[0].value
        return args[0].value, args[1].value


def parse(text: str, file: pathlib.Path, library: RuleLibrary = None,
          file_root: Optional[DataFile] = None) -> List[interfaces.Rule]:
    if library is None:
        library = RuleLibrary()
    result = RuleParsingTree(library, file, file_root)
    file_parser = Lark(action_grammar, parser='lalr', transformer=result)
    cut_marker = "\nExpect"
    try:
        r_list = file_parser.parse(text)
        return r_list
    except Exception as e:
        # way too verbose error... drop stuff from the end
        mesg = str(e)
        if cut_marker in mesg:
            mesg = mesg[0:mesg.index(cut_marker)].strip()
        raise Exception(f"{file.as_posix()}: {mesg}") from None
