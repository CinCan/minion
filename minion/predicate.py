import itertools
import pathlib
from logging import Logger
from typing import Optional, Iterable, List, Dict

from minion import value
from minion.file import DataFile
from minion.value import ValueName, INPUT_NAME, INPUT_VALUE


def unquote(value: str, require_quotes: bool = False) -> Optional[str]:
    if not value:
        return value
    if value.startswith('"') and value.endswith('"'):
        return value[1:-1]
    if value.startswith("'") and value.endswith("'"):
        return value[1:-1]
    if require_quotes:
        return None
    return value


class PredicateValue:
    def __init__(self, name: str = None, files: List[DataFile] = None, string: str = None, boolean: bool = None):
        self.name = name or '(unnamed)'
        self.files = files
        self.string = string
        self.boolean = boolean

    def bool_value(self) -> bool:
        if self.boolean is not None:
            return self.boolean
        if self.string is not None:
            return len(self.string) > 0
        if not self.files:
            return False
        # Do not set self.boolean, as then we start returning boolean values for all calls
        return any([f.bool_value() for f in self.files])

    def value(self) -> str:
        if self.boolean is not None:
            return "True" if self.boolean else ""
        if self.string is not None:
            return self.string
        if not self.files:
            return ''
        self.string = ' '.join([v.value() for v in self.files])
        return self.string


class PredicateValues:
    def __init__(self, logger: Logger, input_name: str):
        self.logger = logger
        self.input_name = input_name
        self.base_name = ''

    def iterate(self) -> Iterable[PredicateValue]:
        raise NotImplementedError()

    def get(self, value_name: str) -> Optional[PredicateValue]:
        return None

    def update(self, value_name: str, value: Optional[PredicateValue]):
        raise NotImplementedError()


class RuleReference:
    def __init__(self, name: ValueName, scope: Optional[ValueName] = None):
        self.name = name
        self.scope = scope
        self._tail_index = name.name.find('/')

    @classmethod
    def new(cls, name: str, scope: Optional[str] = None):
        return RuleReference(ValueName(name), ValueName(scope) if scope else None)

    def head(self) -> ValueName:
        return ValueName(self.name.name[0:self._tail_index]) if self._tail_index > 0 else self.name

    def tail(self) -> Optional['RuleReference']:
        return RuleReference.new(self.name.name[self._tail_index + 1:]) if self._tail_index > 0 else None

    def value_name(self) -> ValueName:
        if self._tail_index > 0:
            return self.tail().value_name()
        return self.name

    def all_names(self, list_to: List[ValueName] = None) -> List[ValueName]:
        ns = [] if list_to is None else list_to
        if self.scope:
            ns.append(self.scope)
        ns.append(self.head())
        tail = self.tail()
        if tail:
            tail.all_names(ns)
        return ns

    def append(self, name: ValueName) -> 'RuleReference':
        return RuleReference(ValueName(self.name.name + '/' + name.name), self.scope)

    def __repr__(self):
        if self.scope:
            return f"{self.scope}:{self.name}"
        else:
            return f"{self.name}"

    def __eq__(self, other) -> bool:
        return isinstance(other, RuleReference) and self.name == other.name and self.scope == other.scope

    def __hash__(self):
        return self.name.__hash__() + (self.scope.__hash__() if self.scope else 0)

    def __lt__(self, other):
        if self.scope:
            if not other.scope:
                return False
            return self.scope.__lt__(other.scope)
        elif other.scope:
            return True
        return self.name.__lt__(other.name)


class PredicateOperation:

    def evaluate(self, values: PredicateValues) -> PredicateValue:
        raise NotImplementedError()

    def list_variables(self) -> Iterable[RuleReference]:
        return []

    def local_values(self, root: DataFile, to_map: Dict[str, List[DataFile]]):
        pass


class PredicateReference(PredicateOperation):
    def __init__(self, reference: RuleReference):
        self.reference = reference

    @classmethod
    def new(cls, variable: str) -> 'PredicateReference':
        parts = variable.partition(':')
        scope = parts[0] if parts[2] else None
        name = parts[2] or parts[0]
        return PredicateReference(RuleReference.new(name, scope))

    def evaluate(self, values: PredicateValues) -> PredicateValue:
        v = values.get(self.reference.name.name)
        v = v or PredicateValue(v.name)  # avoid retuning None
        return v

    def list_variables(self) -> Iterable[RuleReference]:
        return [self.reference]

    def __str__(self) -> str:
        return self.reference.__str__()


class PredicateConstant(PredicateOperation):
    def __init__(self, value: str):
        self.value = value

    def evaluate(self, values: PredicateValues) -> PredicateValue:
        return PredicateValue(string=self.value)

    def __str__(self) -> str:
        return f'"{self.value}"'


class PredicateFixedFile(PredicateOperation):
    def __init__(self, file: DataFile):
        self.file = file

    _value_name = ValueName("file")

    def evaluate(self, values: PredicateValues) -> PredicateValue:
        return PredicateValue(boolean=True)

    def list_variables(self) -> Iterable[RuleReference]:
        return [RuleReference(self._value_name, value.LOCAL)]

    def local_values(self, root: DataFile, to_map: Dict[str, List[DataFile]]):
        to_map[self._value_name.name] = [self.file]

    def __str__(self) -> str:
        return f'"{self.file.as_string()}"'


class PredicateFileName(PredicateOperation):
    def __init__(self, only_input_name=False):
        self.only_input_name = only_input_name

    def evaluate(self, values: PredicateValues) -> PredicateValue:
        name_value = values.get(INPUT_NAME.name)
        if name_value.bool_value() and not self.only_input_name:
            return name_value  # value overrides 'function'
        input_file = values.get(INPUT_VALUE.name)
        res = []
        for f in input_file.files or []:
            res.append(f.name())
        res = PredicateValue(INPUT_NAME.name, string=" ".join(res))
        values.update(INPUT_NAME.name, res)
        return res

    def list_variables(self) -> Iterable[RuleReference]:
        if self.only_input_name:
            return []
        return [RuleReference(INPUT_NAME)]

    def __str__(self) -> str:
        return f'name'


class PredicatePattern(PredicateOperation):
    def __init__(self, base: PredicateOperation, path: PredicateOperation):
        self.base = base
        self.path = path

    def evaluate(self, values: PredicateValues) -> PredicateValue:
        base = self.base.evaluate(values)
        if not base.bool_value():
            values.logger.warning("No value for '%s' in %s", self.base, self)
        path = self.path.evaluate(values).value()
        indexed = []
        for f in base.files:
            indexed.append(f / path)
        res = PredicateValue(base.name, files=indexed)
        values.update(base.name, res)
        return res

    def list_variables(self) -> Iterable[RuleReference]:
        return itertools.chain(self.base.list_variables(), self.path.list_variables())

    def __str__(self) -> str:
        return self.base.__str__() + '/' + self.path.__str__()


class PredicateIndex(PredicateOperation):
    def __init__(self, sub: PredicateOperation):
        self.sub = sub

    def evaluate(self, values: PredicateValues) -> PredicateValue:
        index = self.sub.evaluate(values)
        return index

    def list_variables(self) -> Iterable[RuleReference]:
        return self.sub.list_variables()

    def __str__(self) -> str:
        return f'[{self.sub}]'


class PredicateSimpleMatch(PredicateOperation):
    def __init__(self, value: PredicateOperation, pattern: PredicateOperation):
        self.value = value
        self.pattern = pattern

    def evaluate(self, values: PredicateValues) -> PredicateValue:
        v = self.value.evaluate(values).value()
        pat_v = self.pattern.evaluate(values).value()
        if not pat_v or not pat_v.strip():
            return PredicateValue()
        split = pat_v.split("*")
        i = 0
        off = 0
        len_v = len(v)
        s = split[0]
        len_s = len(s)
        if len_s > 0:
            if len_v < i + len_s or v[i:i + len_s] != s:
                return PredicateValue()
            off += len_s
            i += 1
        while i < len(split):
            s = split[i]
            len_s = len(s)
            if len_s > 0:
                off = v.find(s, off)
                if off < 0:
                    return PredicateValue()
            i += 1
            off += len_s
        if split[-1] == '' or off == len_v:
            return PredicateValue(boolean=True)
        return PredicateValue()

    def list_variables(self) -> Iterable[RuleReference]:
        return itertools.chain(self.value.list_variables(), self.pattern.list_variables())

    def __str__(self) -> str:
        return f'{self.value} // {self.pattern}'


class PredicateEquals(PredicateOperation):
    def __init__(self, left: PredicateOperation, right: PredicateOperation):
        self.left = left
        self.right = right

    def evaluate(self, values: PredicateValues) -> PredicateValue:
        lv = self.left.evaluate(values).value()
        rv = self.right.evaluate(values).value()
        return PredicateValue(boolean=(lv == rv))

    def list_variables(self) -> Iterable[RuleReference]:
        return itertools.chain(self.left.list_variables(), self.right.list_variables())

    def __str__(self) -> str:
        return f'{self.left} == {self.right}'


class PredicateAnd(PredicateOperation):
    def __init__(self, left: PredicateOperation, right: PredicateOperation):
        self.left = left
        self.right = right

    @classmethod
    def sequence(cls, *rules: PredicateOperation) -> PredicateOperation:
        if not rules:
            return PredicateConstant('')
        left = rules[0]
        for right in rules [1:]:
            left = PredicateAnd(left, right)
        return left

    def evaluate(self, values: PredicateValues) -> PredicateValue:
        lv = self.left.evaluate(values)
        if not lv.bool_value():
            return lv
        rv = self.right.evaluate(values)
        return rv

    def list_variables(self) -> Iterable[RuleReference]:
        return itertools.chain(self.left.list_variables(), self.right.list_variables())

    def __str__(self) -> str:
        return f"({self.left} and {self.right})"


class PredicateOr(PredicateOperation):
    def __init__(self, left: PredicateOperation, right: PredicateOperation):
        self.left = left
        self.right = right

    def evaluate(self, values: PredicateValues) -> PredicateValue:
        lv = self.left.evaluate(values)
        if lv.bool_value():
            return lv
        rv = self.right.evaluate(values)
        return rv

    def list_variables(self) -> Iterable[RuleReference]:
        return itertools.chain(self.left.list_variables(), self.right.list_variables())

    def __str__(self) -> str:
        return f"{self.left} or {self.right}"


class PredicateWith(PredicateOperation):
    def __init__(self, rule: RuleReference, sub: PredicateOperation):
        self.rule = rule
        self.sub = sub

    def evaluate(self, values: PredicateValues) -> PredicateValue:
        raise NotImplementedError()

    def list_variables(self) -> Iterable[RuleReference]:
        return itertools.chain([self.rule], self.sub.list_variables())

    def __str__(self) -> str:
        return f"{self.rule} with {self.sub}"


class PredicateSequence(PredicateOperation):
    def __init__(self, left: PredicateOperation, right: PredicateOperation):
        self.left = left
        self.right = right

    @classmethod
    def sequence(cls, *rules: PredicateOperation) -> PredicateOperation:
        if not rules:
            return PredicateConstant('')
        if len(rules) == 1:
            return rules[0]
        left = rules[0]
        for right in rules[1:]:
            left = PredicateSequence(left, right)
        return left

    def evaluate(self, values: PredicateValues) -> PredicateValue:
        lv = self.left.evaluate(values)
        rv = self.right.evaluate(values)
        return PredicateValue(string=lv.value() + rv.value())

    def list_variables(self) -> Iterable[RuleReference]:
        return itertools.chain(self.left.list_variables(), self.right.list_variables())

    def __str__(self) -> str:
        return f"({self.left}{self.right})"


class PredicateNot(PredicateOperation):
    def __init__(self, sub: PredicateOperation):
        self.sub = sub

    def evaluate(self, values: PredicateValues) -> PredicateValue:
        v = self.sub.evaluate(values)
        return PredicateValue(boolean=not v.bool_value())

    def list_variables(self) -> Iterable[RuleReference]:
        return self.sub.list_variables()

    def __str__(self) -> str:
        return f"not {self.sub}"


class PredicateOptional(PredicateOperation):
    def __init__(self, sub: PredicateOperation):
        self.sub = sub

    def evaluate(self, values: PredicateValues) -> PredicateValue:
        return PredicateValue(boolean=True)

    def list_variables(self) -> Iterable[RuleReference]:
        return self.sub.list_variables()

    def __str__(self) -> str:
        return f"not {self.sub}"


class PredicateAll(PredicateOperation):
    def evaluate(self, values: PredicateValues) -> PredicateValue:
        return PredicateValue(string="all")

    def __str__(self) -> str:
        return "all"


class Predicates:
    all = PredicateAll()

    # source file as input
    SOURCE = PredicateReference.new(value.INPUT_VALUE.name)

    # input file name
    IN_FILE_NAME = PredicateFileName()

    # true
    TRUE = PredicateConstant('TRUE')

    # false
    FALSE = PredicateConstant('')

    @classmethod
    def ref(cls, value: str) -> PredicateOperation:
        return PredicateReference.new(value)

    @classmethod
    def string(cls, value: str) -> PredicateOperation:
        return PredicateConstant(value)

    @classmethod
    def string(cls, value: str) -> PredicateOperation:
        return PredicateConstant(value)

    @classmethod
    def optional(cls, sub: PredicateOperation) -> PredicateOperation:
        return PredicateOptional(sub)

    @classmethod
    def equal(cls, left: PredicateOperation, right: PredicateOperation) -> PredicateOperation:
        return PredicateEquals(left, right)

    @classmethod
    def simple_match(cls, value: PredicateOperation, pattern: PredicateOperation) -> PredicateOperation:
        return PredicateSimpleMatch(value, pattern)

    @classmethod
    def not_(cls, sub: PredicateOperation) -> PredicateOperation:
        return PredicateNot(sub)

    @classmethod
    def sequence(cls, *sub: PredicateOperation) -> PredicateOperation:
        if not sub:
            return PredicateConstant('')
        left = sub[0]
        for right in sub[1:]:
            left = PredicateSequence(left, right)
        return left

    @classmethod
    def and_(cls, *sub: PredicateOperation) -> PredicateOperation:
        if not sub:
            return PredicateConstant('')
        left = sub[0]
        for right in sub[1:]:
            left = PredicateAnd(left, right)
        return left

    @classmethod
    def or_(cls, *sub: PredicateOperation) -> PredicateOperation:
        if not sub:
            return PredicateConstant('')
        left = sub[0]
        for right in sub[1:]:
            left = PredicateOr(left, right)
        return left
