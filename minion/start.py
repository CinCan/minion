import argparse
import json
import logging
import multiprocessing
import pathlib
import sys
from typing import List, Dict, Optional, Iterable

from minion.exports import JSONExport, PlantUMLExport, EBNFExport
from minion.file import DataFile
from minion.file_facade import Facade
from minion.file_manager import SubDirectoryFileManager
from minion.file_registry import DefaultFileRegistry
from minion.live_log import LiveLogger
from minion.remote_tracker import RemoteTracker
from minion.rule_library import FileBasedRuleLibrary
from minion.settings import LocalSettings
from minion.value import ValueName


class SubCommand:
    """Base class for command line sub-commands"""
    def __init__(self, command_name: str, parsers):
        self.command_name = command_name
        self.path: Optional[pathlib.Path] = None

    def _argument_files(self, parser):
        """Add input files -argument"""
        parser.add_argument('files', nargs='*', help='Input file(s), can be omitted if defaults')

    def create_facade(self, args, settings: LocalSettings) -> Optional[Facade]:
        """Create file facade"""
        sub_cmd = args.sub_command
        src = args.files
        if not settings.explicit_rule_file and not src and not settings.is_default_set_of_settings():
            paths = FileBasedRuleLibrary.list_rule_files()
            info = "\n".join([p.as_posix() for p in paths])
            sys.stderr.write(f'No rule set specified, built-in rule sets (choose by --rules <name> {sub_cmd}):\n' + \
                             info + '\n')
            return None
        src = src or settings.default_files()
        for s in src:
            if not pathlib.Path(s).exists():
                raise Exception(f"Could not find file '{s}'")
        facade = Facade.in_directory(settings.project_name, settings.work_dir.resolve(),
                                     rule_file=settings.rule_file.as_posix(), sources=src)
        facade.manager.ignore_changes = args.ignore_changes
        facade.manager.rules.rule_timeout = args.rule_timeout
        facade.builder.debug_targets = set([ValueName(t) for t in args.debug_target.split(',') if t])
        facade.scheduler.delay_after_apply = args.delay
        default_threads = 1 if facade.builder.debug_targets else multiprocessing.cpu_count()
        facade.scheduler.max_threads = args.threads if args.threads >= 0 else default_threads
        for name_value in args.define or []:
            name, _, value = name_value.partition('=')
            facade.define(name, value)
        settings.update_defaults(sources=src)

        api_addr = args.api
        api_key = args.api_key
        if api_addr:
            # start tracking API
            RemoteTracker.attach(facade, api_addr, api_key)

        live_log = args.live_log or False
        if live_log:
            facade.listeners.append(LiveLogger(facade))
        return facade

    def run(self, args, settings: LocalSettings):
        """Run the sub command, must be redefined in all concrete sub commands"""
        raise NotImplemented()

    def add(self, commands: Dict[str, 'SubCommand']) -> 'SubCommand':
        """Add the sub command to list of commands"""
        commands[self.command_name] = self
        return self

    def _print_result(self, args, files: Iterable[DataFile]):
        """Print result, either file contents or file names"""
        content = args.content
        listed = set()
        for f in files:
            if content and f.is_file():
                sys.stdout.buffer.write(f.bytes_value())
            else:
                f_out = f.file_output()
                for out in [f for f in f_out if f not in listed]:
                    print(out)
                listed.update(f_out)


class UseCommand(SubCommand):
    def __init__(self, parsers):
        super().__init__('use', parsers)
        p = parsers.add_parser(self.command_name, help='Use given rule set to resolve default targets')
        p.add_argument('--target', '-t', nargs='?', help='Target a specific value(s)')
        self._argument_files(p)

    def run(self, args, settings: LocalSettings):
        facade = self.create_facade(args, settings)
        if not facade:
            return
        if args.target:
            targets = [ValueName(v) for v in args.target.split(' ')]
        else:
            targets = [r.end for r in facade.manager.rules.get_targets()]
        if not targets:
            raise Exception(f"No rules to apply")
        for t in targets:
            if not facade.manager.rules.is_reachable(t):
                raise Exception(f"Unknown target '{t}'")
        facade.start()
        files = facade.find_values(*targets)
        facade.stop()
        if not files:
            ts = ", ".join([t.name for t in targets])
            raise Exception(f"No result files created for targets: {ts}")
        self._print_result(args, files)


class BuildCommand(SubCommand):
    def __init__(self, parsers):
        super().__init__('build', parsers)
        p = parsers.add_parser(self.command_name, help='Build all possible result files')
        self._argument_files(p)

    def run(self, args, settings: LocalSettings):
        facade = self.create_facade(args, settings)
        if not facade:
            return
        facade.start()
        facade.build_all()
        facade.stop()


class GetCommand(SubCommand):
    def __init__(self, parsers):
        super().__init__('get', parsers)
        p = parsers.add_parser(self.command_name, help='Get value by name')
        p.add_argument('target', nargs='?', help='Target a specific value(s)')
        self._argument_files(p)

    def run(self, args, settings: LocalSettings):
        facade = self.create_facade(args, settings)
        if not facade:
            return
        if not args.target:
            all_rules = facade.manager.rules.list_rules()
            all_targets = sorted(set([r.end.name for r in all_rules]))
            sys.stdout.write("\n".join(all_targets))
            return
        targets = [ValueName(v) for v in args.target.split(' ')]
        for t in targets:
            if not facade.manager.rules.is_reachable(t):
                raise Exception(f"Unknown target '{t}'")
        facade.start()
        files = facade.find_values(*targets)
        facade.stop()
        if not files:
            raise Exception(f"Could not create files for target: {args.target}")
        self._print_result(args, files)


class FileCommand(SubCommand):
    def __init__(self, parsers):
        super().__init__('file', parsers)
        p = parsers.add_parser(self.command_name, help='Get specific file')
        p.add_argument('path', help='File name')
        self._argument_files(p)

    def run(self, args, settings: LocalSettings):
        facade = self.create_facade(args, settings)
        if not facade:
            return
        p_file = facade.manager.registry.get_file_by_path(args.path)
        facade.start()
        file = facade.get_data_file(p_file)
        facade.stop()
        if not file:
            raise Exception(f"Could not resolve file: {p_file.as_string()}")
        self._print_result(args, [file])


class CleanCommand(SubCommand):
    def __init__(self, parsers):
        super().__init__('clean', parsers)
        p = parsers.add_parser(self.command_name, help='Clean generated files')
        p.add_argument('-f', '--force', action='store_true', default=False,
                       help='Force full deletion')
        p.add_argument('-d', '--dry', action='store_true', default=False,
                       help='Dry run, only list files which would be deleted')
        p.add_argument('clean_rules', nargs='*', help='Rule sets to clean')

    def run(self, args, settings: LocalSettings):
        project_names = args.clean_rules or [settings.project_name]
        for n in project_names:
            registry = DefaultFileRegistry(settings.work_dir, project_name=n)
            registry.clean(registry.root_file, dry=args.dry, force=args.force)


class ExplainCommand(SubCommand):
    def __init__(self, parsers):
        super().__init__('explain', parsers)
        p = parsers.add_parser(self.command_name, help='Explain how a file is created by rules')
        p.add_argument('file', help='The file')
        p.add_argument('-a', '--all', action='store_true', default=False,
                       help='Include all prints, also to stdout and stderr')

    def run(self, args, settings: LocalSettings):
        registry = DefaultFileRegistry(settings.work_dir, project_name=settings.project_name)
        file = registry.get_file_by_path(args.file)
        registry.explain(sys.stdout, file, with_details=args.all)


class ExportCommand(SubCommand):
    def __init__(self, parsers):
        super().__init__('export', parsers)
        p = parsers.add_parser(self.command_name, help='Export rules as a graph JSON')
        p.add_argument('format', help="Export format ('json', 'plantuml-ad')")
        p.add_argument('--server-url', nargs='?',
                       help="Optional PlantUML server URL (public http://www.plantuml.com/plantuml/img/)")
        p.add_argument('--output', '-o', nargs='?', help="Output file, default to stdout")

    def run(self, args, settings: LocalSettings):
        rules = FileBasedRuleLibrary.locate(settings.rule_file)
        fmt = args.format
        url = args.server_url
        output_file = args.output
        if fmt == 'json':
            js = JSONExport(rules).do_export()
            out = json.dumps(js).encode('ascii')
        elif fmt == 'plantuml-ad':
            if url:
                out = PlantUMLExport(rules).export_image(url)
            else:
                # NOTE: Copy-paste into PlantUML activity diagram
                out = PlantUMLExport(rules).export_text().encode('ascii')
        elif fmt == 'ebnf':
            # NOTE: Copy-paste into Railroad Diagram Generator (this is quite obsolete view)
            out = EBNFExport(rules).do_export().encode('ascii')
        else:
            raise Exception(f"Unknown export format '{fmt}'")
        if output_file:
            with open(output_file, 'wb') as f:
                f.write(out)
        else:
            sys.stdout.buffer.write(out)


class HierarchyCommand(SubCommand):
    def __init__(self, parsers):
        super().__init__('ls', parsers)
        p = parsers.add_parser(self.command_name, help='List input and output files')
        p.add_argument('file', nargs='?', help='Start file')

    def run(self, args, settings: LocalSettings):
        registry = SubDirectoryFileManager(DefaultFileRegistry(settings.work_dir,
                                                               project_name=settings.project_name))
        file = registry.get_file_by_path(args.file) if args.file else registry.root_file
        registry.print_hierarchy(sys.stdout, file)


class HelpCommand(SubCommand):
    def __init__(self, arg_parser, parsers):
        super().__init__('help', parsers)
        parsers.add_parser(self.command_name)
        self.arg_parser = arg_parser

    def run(self, args, settings: LocalSettings):
        self.arg_parser.print_help()


def main(args: List[str] = None):
    m_parser = argparse.ArgumentParser()
    m_parser.add_argument("-l", "--log", dest="log_level", choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'],
                          help="Set the logging level", default=None)
    m_parser.add_argument('--live-log', action='store_true', help='Show live log in console')
    m_parser.add_argument('-q', '--quiet', action='store_true', help='Be quite quiet')
    m_parser.add_argument('-c', '--content', action='store_true', help='Return file content, when applicable')
    m_parser.add_argument('--work-dir', '-C', nargs='?', help='Change the working directory')
    m_parser.add_argument('--rules', '-r', nargs='?', help='Select the rule set file, default minion.rules')
    m_parser.add_argument('--threads', type=int, default=-1, help='Number of parallel threads to use')
    m_parser.add_argument('--rule-timeout', '-t', type=float, default=300,
                          help='Rule timeout in seconds, default is 300 (5 mins)')
    m_parser.add_argument('--define', '-D', nargs='?', action='append', help='Define a value, repeat as needed')
    m_parser.add_argument('--api', help="Open web API to '[ip:]port'")
    m_parser.add_argument('--api-key', help="Specify API key")
    m_parser.add_argument('--ignore-changes', action='store_true', help='Ignore changes in rules and input files')
    m_parser.add_argument('--debug-target', default="", help='Debug creation of the listed target(s)')
    m_parser.add_argument('--delay', type=int, default=0, help='Delay after each rule, seconds')

    subparsers = m_parser.add_subparsers(dest='sub_command')

    sub_commands: Dict[str, SubCommand] = {}
    UseCommand(subparsers).add(sub_commands)
    BuildCommand(subparsers).add(sub_commands)
    ExportCommand(subparsers).add(sub_commands)
    GetCommand(subparsers).add(sub_commands)
    FileCommand(subparsers).add(sub_commands)
    CleanCommand(subparsers).add(sub_commands)
    ExplainCommand(subparsers).add(sub_commands)
    HierarchyCommand(subparsers).add(sub_commands)
    sub_commands[''] = HelpCommand(m_parser, subparsers).add(sub_commands)

    args = m_parser.parse_args(sys.argv[1:] if args is None else args)

    work_dir = None
    if args.work_dir:
        work_dir = pathlib.Path(args.work_dir)
    settings = LocalSettings(pathlib.Path(args.rules) if args.rules else None, work_dir=work_dir)

    log_level = args.log_level if args.log_level else ('WARNING' if args.quiet else 'INFO')
    if log_level not in {'DEBUG'}:
        sys.tracebacklimit = 0

    live_log = args.live_log or False
    log_file = None
    if live_log:
        # make room for live log by redirecting log to file
        log_file = settings.get_log_file()
        if log_file.exists():
            log_file.replace(log_file.as_posix() + '.old')

    logging.basicConfig(format='%(asctime)s %(message)s', level=getattr(logging, log_level), datefmt='%H:%M:%S',
                        filename=log_file)

    run_command = sub_commands.get(args.sub_command or '')
    if run_command:
        run_command.run(args, settings)
    else:
        raise Exception(f"Unknown sub command: {args.sub_command}")
