import hashlib
import pathlib
import threading
from datetime import datetime
from typing import Dict, Tuple, Optional, Iterable, List


class DataFileCache:
    def __init__(self):
        self.files: Dict[Tuple[pathlib.Path, pathlib.Path], DataFile] = {}
        self.lock = threading.Condition()

    def get_or_create(self, root: pathlib.Path, path: pathlib.Path) -> 'DataFile':
        key = (root, path)
        with self.lock:
            file = self.files.get(key)
            if file:
                return file
            file = DataFile(root, path)
            self.files[key] = file
        return file


data_file_cache = DataFileCache()


class DataFile:
    def __init__(self, root: pathlib.Path, path: pathlib.Path = None):
        """Constructor, please USE THE new() method!"""
        self.root = root if path is not None else pathlib.Path('/')
        self.path = root if path is None else path
        self.relative_path = self.path if self.root.name == '' else self.path.relative_to(self.root)
        self.digest: Optional[str] = None

    @classmethod
    def new(cls, root: pathlib.Path, path: pathlib.Path = None) -> 'DataFile':
        return data_file_cache.get_or_create(root, path or root)

    @classmethod
    def root_file(cls, root: pathlib.Path) -> 'DataFile':
        p = root.resolve()
        return data_file_cache.get_or_create(p, p)

    def as_string(self) -> str:
        return self.relative_path.as_posix()

    def file_output(self) -> List[str]:
        if self.is_file():
            return [self.as_string()]
        return [f.as_string() for f in self.list_files(recurse=True)]

    def name(self) -> str:
        return self.path.name

    def exists(self) -> bool:
        return self.path.exists()

    def is_root(self) -> bool:
        return self.path == self.root

    def is_file(self) -> bool:
        return self.path.is_file()

    def is_dir(self) -> bool:
        return self.path.is_dir()

    def is_empty(self) -> bool:
        return self.path.is_file() and self.path.stat().st_size == 0

    def mtime(self) -> datetime:
        return datetime.fromtimestamp(self.path.stat().st_mtime)

    def value(self, value_default: Optional[str] = None, dir_default: Optional[str] = '<dir>') -> Optional[str]:
        if self.is_dir():
            return dir_default
        if not self.is_file():
            return value_default
        with self.path.open("r") as f:
            value = f.read()[0:1024]  # limit returned data -- not read data :(
        return value.strip()  # strip white space

    def bytes_value(self) -> Optional[bytes]:
        if self.is_dir():
            return None
        if not self.is_file():
            return None
        with self.path.open("rb") as f:
            value = f.read()
        return value

    def bool_value(self) -> bool:
        if self.is_dir():
            return True  # directory is True
        if not self.is_file():
            return False
        return self.path.stat().st_size > 0  # non-empty file is True

    def parent(self) -> Optional['DataFile']:
        if self.is_root():
            return None  # cannot go toward root any more
        else:
            return DataFile.new(self.root, self.path.parent)

    def list_files(self, recurse=False) -> Iterable['DataFile']:
        if not self.path.is_dir():
            return []
        file_list = []
        for f in sorted(self.path.iterdir()):
            if self.is_root() and f.name == '.minion':
                continue  # do not visit .dirs in the root
            df = DataFile.new(self.root, f)
            if recurse and f.is_dir():
                f_list = df.list_files(recurse=recurse)
                if not f_list:
                    f_list = [df]
                file_list.extend(f_list)
            else:
                file_list.append(df)
        return file_list

    def with_path(self, path: pathlib.Path) -> 'DataFile':
        return DataFile.new(self.root, path)

    def sha256_digest(self) -> str:
        if self.digest:
            return self.digest
        hash = hashlib.sha256()
        with self.path.open('rb') as f:
            read = f.read(8196)
            while read:
                hash.update(read)
                read = f.read(8196)
        self.digest = hash.hexdigest()
        return self.digest

    def __truediv__(self, other: str) -> Optional['DataFile']:
        return DataFile.new(self.root, self.path / other)

    def __repr__(self) -> str:
        return self.as_string()

    def __eq__(self, other) -> bool:
        return isinstance(other, DataFile) and self.path == other.path

    def __hash__(self):
        return self.path.__hash__()

    def __lt__(self, other):
        return self.path.__lt__(other.path)
