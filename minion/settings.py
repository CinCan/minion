import json
import pathlib
from typing import Optional, List, Dict


class LocalSettings:
    def __init__(self, rule_file: Optional[pathlib.Path], work_dir: pathlib.Path = None, directory: pathlib.Path = None):
        self.explicit_rule_file = rule_file is not None
        self.rule_file = rule_file or pathlib.Path('minion.rules')
        self.project_name = self.rule_file.name
        self.work_dir = work_dir or pathlib.Path()
        self.settings_dir = directory or (self.work_dir / '.minion' / self.project_name)
        self.settings_file = self.settings_dir / 'settings'
        self.settings: Dict[str, str] = {}
        if self.settings_file.is_file():
            with self.settings_file.open('r') as f:
                self.settings = json.load(f)

    __sources = 'sources'

    def __ensure_directory(self):
        self.settings_dir.mkdir(parents=True, exist_ok=True)

    def is_default_set_of_settings(self) -> bool:
        return self.__sources in self.settings

    def default_files(self) -> List[str]:
        v = self.settings.get(self.__sources)
        # go without source files, if none stored
        return v or []

    def update_defaults(self, sources: List[str]):
        if sources:
            self.settings[self.__sources] = sources
        elif self.__sources in self.settings:
            del self.settings[self.__sources]
        self.__ensure_directory()
        with self.settings_file.open('w') as f:
            json.dump(self.settings, f)

    def get_log_file(self) -> pathlib.Path:
        self.__ensure_directory()
        return self.settings_dir / 'log'
